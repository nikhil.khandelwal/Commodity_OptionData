import pandas
import sqlite3
import re
import datetime
from monthdelta import monthdelta
import math as m
from scipy import stats
from time import strptime
from calendar import monthrange

# ==DATABASE===================================================================
conn = sqlite3.connect('OPTtest.db')
cursor = conn.execute("SELECT Symbol, Expiry_Month, Expiry_Year, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt, IV, UnderlyingPrice, FilledData from OPTION")
cursor.execute('drop table if exists OPTION')

conn.execute('''CREATE TABLE OPTION
          (Symbol           TEXT    NOT NULL,
          Expiry_Year     TEXT    NOT NULL,
          Expiry_Month      TEXT    NOT NULL,
          Strike            INT    NOT NULL,  
          OptionType      TEXT    NOT NULL,
          Date            TEXT     NOT NULL,
          Time           TEXT  NOT NULL,
          Open            FLOAT     NOT NULL,
          High           FLOAT     NOT NULL,
          Low           FLOAT     NOT NULL,
          Close            FLOAT     NOT NULL,
          Volume           FLOAT     NOT NULL,
          OpenInt          FLOAT       NOT NULL,
          IV               FLOAT     NOT NULL, 
          UnderlyingPrice  FLOAT     NOT NULL,
          FilledData       TEXT     NOT NULL);''')
# =============================================================================

io = pandas.read_csv('C:/Users/DELL/.spyder-py3/imba files/jan2016_to_feb2017/NSEF&O_09012017.csv')
#io = pandas.read_csv('C:/Users/DELL/.spyder-py3/imba files/jan2016_to_feb2017/NSEFO_10012017.csv')

for i in range(0,len(io)):
    d=io['Date'][i].split("/")
    t=io['Time'][i].split(":")
    dt=datetime.datetime(int(d[2]), int(d[1]), int(d[0]), int(t[0]), int(t[1]), int(t[2]))
    time=dt.time()   
    date=dt.date()
    
    if(io['Ticker'][i][-2:]=='-I' or io['Ticker'][i][-3:]=='-II' or io['Ticker'][i][-4:]=='-III'):
        a=io['Ticker'][i].split("-")
        
        if(io['Ticker'][i][-2:]=='-I'):
            dt=dt+monthdelta(1)
        else:
            if(io['Ticker'][i][-3:]=='-II'):
                dt=dt+monthdelta(2)
            elif (io['Ticker'][i][-4:]=='-III'):
                dt=dt+monthdelta(3)
            else:
                print (i, io['Ticker'][i][-3:])

        conn.execute("INSERT INTO OPTION (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                                         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(a[0], dt.year, dt.strftime("%b"), 0, 'FUT', str(date), str(time), str(io['Open'][i]), str(io['High'][i]), str(io['Low'][i]), str(io['Close'][i]), str(io['Volume'][i]), str(io['Open Interest'][i]), 0.0, 0.0, 'NO')); 
    else:
        r = re.compile("([a-zA-Z]*\-?\&?[a-zA-Z]+)([0-9]+)([a-zA-Z]+)([0-9]*\.?[0-9]+)([a-zA-Z]+)")        
        a=[r.match(io['Ticker'][i]).groups()]
        if(a[0][4]=='PE'):
            op='PUT'
        elif(a[0][4]=='CE'):
            op='CALL'
        else:
            print("Invalid Option Type",i)
        if(a[0][0]=='BANKNIFTY'):
            conn.execute("INSERT INTO OPTION (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                                     VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(a[0][0], 2000+int(a[0][3][0:2]), a[0][1]+a[0][2], a[0][3][2:], op, str(date), str(time), str(io['Open'][i]), str(io['High'][i]), str(io['Low'][i]), str(io['Close'][i]), str(io['Volume'][i]), str(io['Open Interest'][i]), 0.0, 0.0, 'NO')); 

        else:    
            conn.execute("INSERT INTO OPTION (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                                     VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(a[0][0], 2000+int(a[0][1]), a[0][2], a[0][3], op, str(date), str(time), str(io['Open'][i]), str(io['High'][i]), str(io['Low'][i]), str(io['Close'][i]), str(io['Volume'][i]), str(io['Open Interest'][i]), 0.0, 0.0, 'NO')); 

conn.commit()

cursor.execute("SELECT DISTINCT Symbol FROM OPTION")
d=cursor.fetchall()
#if not date_list:
date_list=[]
def Missing_timestamp(op):
    for i in range(0,len(d)):
        cursor.execute("SELECT DISTINCT Expiry_Month FROM OPTION WHERE Symbol = ? AND OptionType = ? AND Date = ?", (d[i][0],op,str(date)))
        ex_mon=cursor.fetchall()

        
        for j in range(0,len(ex_mon)):
            a=ex_mon[j][0]
            cursor.execute("SELECT DISTINCT Strike FROM OPTION WHERE Symbol = ? AND Expiry_Month = ? AND OptionType = ? AND Date = ?", (d[i][0],a,op,str(date)))
            st=cursor.fetchall()    
            
            for k in range(0,len(st)):  
                tm=datetime.datetime(int(io['Date'][i][6:10]),int(io['Date'][i][3:5]),int(io['Date'][i][0:2]),9,15,59)
                if(str(tm.date()) not in date_list):
                    date_list.append(str(tm.date()))
                while(str(tm.time())<='15:30:59'):
                    cursor.execute("SELECT Symbol FROM OPTION WHERE Symbol = ? AND Expiry_Month = ? AND Strike = ? AND Time = ? AND OptionType = ? AND Date = ? ", (d[i][0],a,st[k][0],str(tm.time()),op,str(date)))
                    data=cursor.fetchall()  
                    
                    if not data:
                        if(str(tm.time())=='09:15:59'):
                            t=tm                            
                            t=tm+datetime.timedelta(days=-1)
                            while(str(t.date()) not in date_list and str(t.date())>'2016-01-01'):
                                t=t+datetime.timedelta(days=-1)   
                            cursor.execute("SELECT * FROM OPTION WHERE Symbol = ? AND Expiry_Month = ? AND Strike = ? AND Time = ? AND OptionType = ? AND Date = ?", (d[i][0],a,st[k][0],'15:30:59',op,str(t.date())))
                            data=cursor.fetchall()
                            if not data:     
                                if(tm.month>strptime(a,'%b').tm_mon):
                                    exp_yr=tm.year+1
                                else:
                                    exp_yr=tm.year
                                cursor.execute("INSERT INTO OPTION (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                                               VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(d[i][0], exp_yr, a, st[k][0], op, str(tm.date()), str(tm.time()), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 'YES')); 
                                conn.commit()               
                            else:
                                cursor.execute("INSERT INTO OPTION (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt, IV, UnderlyingPrice, FilledData) \
                                               VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(data[0][0], data[0][1], data[0][2], data[0][3], data[0][4], str(tm.date()), str(tm.time()), data[0][7], data[0][8], data[0][9], data[0][10], 0.0, 0.0, 0.0, 0.0, 'YES')); 
                                conn.commit() 
                        else:
                            cursor.execute("SELECT * FROM OPTION WHERE Symbol = ? AND Expiry_Month = ? AND Strike = ? AND Time = ? AND OptionType = ? AND Date = ?", (d[i][0],a,st[k][0],str((tm - datetime.timedelta(hours=1/60)).time()),op,str(tm.date())))
                            data=cursor.fetchall()  
                            cursor.execute("INSERT INTO OPTION (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                                               VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(data[0][0], data[0][1], data[0][2], data[0][3], data[0][4], str(tm.date()), str(tm.time()), data[0][7], data[0][8], data[0][9], data[0][10], 0.0, 0.0, 0.0, 0.0, 'YES')); 
                            conn.commit()               
                                
                    tm = tm + datetime.timedelta(hours=1/60)
    
Missing_timestamp('CALL')
Missing_timestamp('PUT')
Missing_timestamp('FUT')

            



def update(op):
    for i in range(0,len(d)):
        cursor.execute("SELECT DISTINCT Expiry_Month FROM OPTION WHERE Symbol = ? AND OptionType = ? AND Date = ?", (d[i][0],op,str(date)))
        ex_mon=cursor.fetchall()
        for j in range(0,len(ex_mon)):
            cursor.execute("SELECT DISTINCT Strike FROM OPTION WHERE Symbol = ? AND Expiry_Month = ? AND OptionType = ? AND Date = ?", (d[i][0],ex_mon[j][0],op,str(date)))
            st=cursor.fetchall()    
            for k in range(0,len(st)):  
                tm=datetime.datetime(int(io['Date'][i][6:10]),int(io['Date'][i][3:5]),int(io['Date'][i][0:2]),9,15,59)
                tdf=375
                while(str(tm.time())<='15:30:59'):
                    cursor.execute("SELECT Close FROM OPTION WHERE Symbol = ? AND Expiry_Month = ? AND Time = ? AND OptionType = ? AND Date = ?",(d[i][0],((tm+monthdelta(1)).strftime("%b")),str(tm.time()),'FUT',str(tm.date())))
                    data=cursor.fetchall()   
                    cursor.execute("SELECT Close FROM OPTION WHERE Symbol = ? AND Expiry_Month = ? AND Time = ? AND OptionType = ? AND Date = ? AND Strike = ?",(d[i][0],ex_mon[j][0],str(tm.time()),op,str(tm.date()),st[k][0]))
                    close=cursor.fetchall()
                    cursor.execute('''UPDATE OPTION SET IV = ?, UnderlyingPrice =? WHERE Symbol = ? AND Expiry_Month = ? AND Strike = ? AND Time = ? AND OptionType = ? AND Date = ? ''',(calc_vix(data[0][0],op,st[k][0],(calc_days_left(tm,ex_mon[j][0])+float(tdf/375)),close[0][0]), data[0][0], d[i][0],ex_mon[j][0],st[k][0],str(tm.time()),op,str(tm.date())))
                    conn.commit()
                    tdf-=1
                    tm = tm + datetime.timedelta(hours=1/60)

def black_scholes (cp, s, k, t, v, rf=0.010, div=0):
        """ Price an option using the Black-Scholes model.
        s: initial stock price
        k: strike price
        t: expiration time
        v: volatility
        rf: risk-free rate
        div: dividend
        cp: +1/-1 for call/put
        """
        t = float(t)/365.0
        d1 = (m.log(s/k)+(rf-div+0.5*m.pow(v,2))*t)/(v*m.sqrt(t))
        d2 = d1 - v*m.sqrt(t)
        optprice = (cp*s*m.exp(-div*t)*stats.norm.cdf(cp*d1)) - (cp*k*m.exp(-rf*t)*stats.norm.cdf(cp*d2))
        return optprice
    
def calc_days_left(d,search_month):
    """
    Input date: 'yyyy-mm-dd'
    return days left to next nifty Expiry
    """
    nd=0
    if (search_month!=d.strftime("%b")):
        while(search_month!=d.strftime("%b").upper()):
            a,max_days = monthrange(d.year, d.month)
            nd+=max_days
            d=d+monthdelta(1)
            
    date=str(d.date())    
    split_date = date.split('-')
    if len(split_date)!=3:
        print ('Check date')
        return -1
    month = int(split_date[1])
    year = int(split_date[0])
    day = int(split_date[2])
    a,max_days = monthrange(year, month)
    for i in range(1,max_days+1): 
        weekday = datetime.date(year,month,i).weekday()
        if weekday==3:
            last_thurs = i
    ndays = (datetime.date(year,month,last_thurs)- datetime.date(year,month,day)).days
    if ndays<1:
        if month<12:
            month+=1
            a,max_days = monthrange(year, month)
            for i in range(1,max_days+1): 
                weekday = datetime.date(year,month,i).weekday()
                if weekday==3:
                    last_thurs = i
            ndays = (datetime.date(year,month,last_thurs)- datetime.date(year,month-1,day)).days
        else:
            month=1
            year+=1
            a,max_days = monthrange(year, month)
            for i in range(1,max_days+1): 
                weekday = datetime.date(year,month,i).weekday()
                if weekday==3:
                    last_thurs = i
            ndays = (datetime.date(year,month,last_thurs)- datetime.date(year-1,12,day)).days 
            
#    if search_month == "next":
#        a,max_days = monthrange(year, month)
#        ndays2 = calc_days_left(str(year)+'-'+str())
#        return ndays2+ max_days
    return ndays+nd

def calc_vix(underlyingValue,callPut,strikePrice,daysToExpiry,price):
    for j in range(1,100):
        i= float(j)/100      
        if callPut=='CALL':
            curP =  black_scholes(1,underlyingValue,float(strikePrice),daysToExpiry,i)
        elif callPut=='PUT':
            curP = black_scholes(-1,underlyingValue,float(strikePrice),daysToExpiry,i)  
        if curP>price:
            break
    return i

update('CALL') 
update('PUT')                   
   

conn.close()

