# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 12:49:01 2018

@author: abc
"""
from utilities import calc_expiry, get_option_code
import numpy as np
from datetime import datetime, timedelta
from kiteconnect import KiteConnect
import pandas as pd
tradingsym="NIFTY 50"
percentile = 0.06

access_token = "jxih4qezvdfyljmbaga663c144rju97f"
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)
from_date_now = datetime.strftime(datetime.now().replace(year = datetime.now().year-5), "%Y-%m-%d")
to_date_now = datetime.strftime(datetime.now(), "%Y-%m-%d")
now = datetime.now()
exch2 ="NSE"
def calc_change(a, b):
    try:
        r = (float(a)/b - 1)*100
    except:
        r = 0
    return r
indexData  = pd.DataFrame(kite.historical(get_option_code(tradingsym, exch2),from_date_now, to_date_now, 'day'))
indexData['Change'] = 0
end = calc_expiry(now)
nday = end.weekday() -now.weekday()
workingdays = np.busday_count( now, end)
#workingdays = 20
for i in range(workingdays, len(indexData)):
    indexData.loc[i, 'Change'] = calc_change(indexData.loc[i,'close'], indexData.loc[i-workingdays,'close'])
bear = indexData[indexData['Change']<=0][['date','Change']]
bull = indexData[indexData['Change']>=0][['date','Change']]
max_fall = int(min(bear['Change']))
max_rise = int(max(bull['Change']))
bear_frq =bear.apply(pd.Series.value_counts, bins=np.arange(max_fall, 0, 0.25))
bull_frq =bull.apply(pd.Series.value_counts, bins=np.arange(0, max_rise, 0.25))
bull_frq['cum_sum'] = bull_frq.Change.cumsum()/sum(bull_frq.Change)
bear_frq['cum_sum'] = bear_frq.Change.cumsum()/sum(bear_frq.Change)
prob_rise = bull_frq[bull_frq['cum_sum']>1-percentile].index[0]
prob_fall = bear_frq[bear_frq['cum_sum']<percentile].index[-1]
price = kite.quote(exch2, tradingsym)['last_price']
vix = kite.historical(264969, datetime.strftime(datetime.now() - timedelta(days = 25), "%Y-%m-%d"),to_date_now,'minute')
vix_cur = vix[-1]['close']
vix_l = []
for i in vix:
    vix_l.append(i['close'])
vix_avg = sum(vix_l) / float(len(vix_l))
print price*(1 + prob_rise/100.0)
print price*(1 + prob_fall/100.0)
