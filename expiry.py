import sqlite3
import pandas
from datetime import datetime,timedelta
import csv

io = pandas.read_csv('Expiry.csv')
conn = sqlite3.connect('t1.db')
conn.execute('''CREATE TABLE Exp
         (Date           TEXT    NOT NULL,
         expiry            INT     NOT NULL);''')

cursor = conn.execute("SELECT Date, expiry from Exp")
from_date=datetime.strptime(io['Date'][0], "%Y-%m-%d")
to_date=datetime.strptime(io['Date'][len(io)-1], "%Y-%m-%d")

i=0
while(from_date!=to_date and i<len(io)):
    if(io['Date'][i]==str(from_date.date())):
        conn.execute("INSERT INTO Exp(Date, expiry) \
                     VALUES(?,?)",(io['Date'][i],int(io['Expiry'][i])));
        i+=1
    else:
        conn.execute("INSERT INTO Exp(Date, expiry) \
                     VALUES(?,?)",(str(from_date.date()),0));
    from_date+=timedelta(days=1)                 
conn.commit()

cursor = conn.execute("SELECT Date, expiry from Exp ORDER BY Date ASC")
f=open('expiryyy.csv','w+', newline='') # open the csv data file

writer = csv.writer(f)
head=('Date', 'Expiry')
writer.writerow(head)

for row in cursor:
    writer.writerow(row)

f.close()

conn.close()