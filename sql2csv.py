import sqlite3
import csv

conn = sqlite3.connect('test1.db')

cursor = conn.execute("SELECT Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume from ALU ORDER BY Date,Time ASC")

f=open('NICKELL.csv','w+', newline='') # open the csv data file

writer = csv.writer(f)
head=('Symbol', 'Date', 'Time', 'Open', 'High', 'Low', 'Close', 'OpenInt', 'Volume')
writer.writerow(head)

for row in cursor:
    writer.writerow(row)

f.close()

