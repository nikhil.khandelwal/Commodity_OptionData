import pandas
import datetime
import sqlite3

io = pandas.read_csv('nickel_edit.csv')

####DATABASE####### 
conn = sqlite3.connect('test1.db')
cursor = conn.execute("SELECT Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume from ALU")
cursor.execute('drop table if exists ALU')

conn.execute('''CREATE TABLE ALU
         (Symbol           TEXT    NOT NULL,
         Date            INT     NOT NULL,
         Time           INT  NOT NULL,
         Open            INT     NOT NULL,
         High           INT     NOT NULL,
         Low           INT     NOT NULL,
         Close            INT     NOT NULL,
         OpenInt            INT     NOT NULL,
         Volume           FLOAT     NOT NULL);''')


#####DATABASE
a=0 #STARTING INDEX OF CSV FILE
if(io['Time'][a]<160000):
    t=100000        
else:
    if(io['Time'][a]>183500):
        t=183000
    else:    
        t=io['Time'][a]    #Initial Time
    
d=str(io['Date'][a]) #CSV File Starting  Date

#Cdate : Auto-incremented date
cdate=datetime.datetime(2000+int(d[0:2]),int(d[2:4]),int(d[4:6]))
cdate=cdate.date()
i=a
for i in range(a,len(io)):
    d=str(io['Date'][i])
    date=datetime.datetime(2000+int(d[0:2]),int(d[2:4]),int(d[4:6]))
    date=date.date()    
    
    #if((cdate.day==14 and cdate.month==4 and cdate.year==2017) and (cdate.day==3 and cdate.month==4 and cdate.year==2015) and (cdate.day==25 and cdate.month==3 and cdate.year==2016) and (cdate.day==26 and cdate.month==1) and (cdate.day==15 and cdate.month==8) and (cdate.day==2 and cdate.month==10) and (cdate.day==25 and cdate.month==12)):
    #    cdate = cdate + datetime.timedelta(days=1)            
        
    
    if(i>=1):
        if((165500<=io['Time'][i]<=183500) and io['Time'][i-1]>io['Time'][i]):
            if(io['Time'][i-1]==(170000 or 232900)):
                t=io['Time'][i] 
        
    
    if(i>=1 and t!=100000):
        if(io['Time'][i]<io['Time'][i-1] and t<=232900 ):
            while(io['Time'][i]!=t and t<=232900 and cdate!=date):
                
                if not(((165500<=io['Time'][i]<=180500) and io['Time'][i-1]>io['Time'][i] and io['Time'][i-1]==(170000 or 232900)) or (100000<=io['Time'][i]<160000 and io['Time'][i-1]==170000)):
                #if not(((165500<=io['Time'][i]<=183500) and (io['Time'][i-1]==232900 or 165500<=io['Time'][i-1]<183500)) or (100000<=io['Time'][i]<160000 and (io['Time'][i-1]==232900 or 165500<=io['Time'][i-1]<183500))):
                    #print('Date:',cdate,'Time:',t) ##Prints the missing timestamp  
                    
                    conn.execute("INSERT INTO ALU (Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume) \
                                     VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",(io['Symbol'][i], str(cdate), str(t), 0, 0, 0, 0, 0, 0.0)); 
                 
                if(t==232900): #Next day
                    t=100000
                    cdate = cdate + datetime.timedelta(days=1)
                else:
                    if((t%10000)/100==59.0): #Next hour
                        t=t+4100
                    else:
                        t=t+100 #minute
                if(cdate.weekday()==5):
                    cdate = cdate + datetime.timedelta(days=2) 
                    
                if((cdate.day==14 and cdate.month==4 and cdate.year==2017) or (cdate.day==3 and cdate.month==4 and cdate.year==2015) or (cdate.day==25 and cdate.month==3 and cdate.year==2016) or (cdate.day==26 and cdate.month==1) or (cdate.day==15 and cdate.month==8) or (cdate.day==2 and cdate.month==10) or (cdate.day==25 and cdate.month==12)):
                    cdate = cdate + datetime.timedelta(days=1)            
        
            if(t==232900):
                t=100000
                cdate = cdate + datetime.timedelta(days=1)
    
            if(cdate.weekday()==5):
                cdate = cdate + datetime.timedelta(days=2)
                
    
    while(cdate!=date):     #Missing day
        if not (cdate.weekday()>=5): 
            #Holiday Constaint (doesn't include Good friday)
            if((cdate.day!=14 and cdate.month!=4 and cdate.year!=2017) and (cdate.day!=3 and cdate.month!=4 and cdate.year!=2015) and (cdate.day!=25 and cdate.month!=3 and cdate.year!=2016) and (cdate.day!=26 and cdate.month!=1) and (cdate.day!=15 and cdate.month!=8) and (cdate.day!=2 and cdate.month!=10) and (cdate.day!=25 and cdate.month!=12)):
                #print(cdate)
                conn.execute("INSERT INTO ALU (Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume) \
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",(io['Symbol'][i], str(cdate), 0, 0, 0, 0, 0, 0, 0.0)); 
                 
        cdate = cdate + datetime.timedelta(days=1)

     
    if(cdate==date):    
        while(io['Time'][i]!=t and t<=232900 ):
            #Morning/Evening Session Constraint
            if(i==0):
                conn.execute("INSERT INTO ALU (Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume) \
                             VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",(io['Symbol'][i], str(cdate), str(t), 0, 0, 0, 0, 0, 0.0)); 
                
            else:    
                if not(((165500<=io['Time'][i]<=180500) and io['Time'][i-1]>io['Time'][i]) or (100000<=io['Time'][i]<165900 and io['Time'][i-1]==170000)):
                #print('Date:',cdate,'Time:',t)    
                     conn.execute("INSERT INTO ALU (Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume) \
                                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",(io['Symbol'][i], str(cdate), str(t), 0, 0, 0, 0, 0, 0.0)); 
                 
            if((t%10000)/100==59.0):
                t=t+4100
            else:
                t=t+100
    
            
    
        if(t==232900):
            t=100000
            cdate = cdate + datetime.timedelta(days=1)
            
        
        else:
        
            if((t%10000)/100==59.0):
                t=t+4100
            else:
                t=t+100
    
    if(cdate.weekday()==5): 
        cdate = cdate + datetime.timedelta(days=2)

conn.commit()        

#CSV into Database
'''
with open('DataForCRUDEOIL1.csv','r') as fin: 
    dr = csv.DictReader(fin)
        
    to_db = [(i['Symbol'], str(i['Date']), str(i['Time']), str(i['Open']), str(i['High']), str(i['Low']), str(i['Close']), str(i['OpenInt']), str(i['Volume'])) for i in dr]
cursor.executemany("INSERT INTO ALU (Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);", to_db)
    
conn.commit()
'''

cursor = conn.execute("SELECT Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume from ALU ")

for row in cursor: 
   print (row)
print("done") 
#print (pandas.read_sql_query("SELECT * FROM ALU ", conn))
conn.commit()
conn.close()




