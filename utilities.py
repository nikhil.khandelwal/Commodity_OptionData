# -*- coding: utf-8 -*-
"""
Created on Mon Jan 08 19:27:23 2018

@author: abc
"""
import math as m
from scipy.stats import norm
from scipy import stats
from datetime import datetime, timedelta, date
from calendar import monthrange
import xlrd
from kiteconnect import KiteConnect
    #import openpyxl
import pandas as pd
import numpy as np
import time
import nsepy
from time import mktime
import re
import socket

class loginObject(object):
    def __init__(self, kite, access_token):
        self.kite =  kite
        self.access_token = access_token
        
class tradeObject(object):
    def __init__(self, tradingsym, exch, side, quant, orderType, prod):
        self.tradingsym =  tradingsym
        self.exch = exch
        self.side = side
        self.quant = quant
        self.orderType = orderType
        self.prod = prod
    def addDetails(self,status, orderid, ordertime, orderprice):
        self.status = status
        self.orderid = orderid
        self.ordertime = ordertime
        self.orderprice = orderprice

def mail_alerts (mailid,msg):
	#mailid = ["gupta.abhishek888@gmail.com","buysellmarketalerts@gmail.com"]
	#len = 10  shows 10 min try
	#msg = signal of buying one contract of BankNIFTY 

    import smtplib
    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        print (msg)
#		 server.login("buysellmarketalerts@gmail.com", "13573616cc")
#		 server.sendmail("buysellmarketalerts@gmail.com",mailid, msg)
#    	 server.quit()
    except:
        print("Netfail")
         
def find_vol(target_value, call_put, S, K, T, r):
    """
    Input parameters
    target_value: A float, close price
    call_put: A string,"c"/"p"
    S: A float, future's close price 
    K:A float,strike
    T: time in years
    r: A float,0.1
    
    Output
    :Return sigma: A float
    """
    MAX_ITERATIONS = 100
    PRECISION = 1.0e-5

    sigma = 0.5
    for i in range(0, MAX_ITERATIONS):
        price = bs_price(call_put, S, K, T, r, sigma)
        vega = bs_vega(call_put, S, K, T, r, sigma)

        price = price
        diff = target_value - price  # our root


        if (abs(diff) < PRECISION):
            return sigma
        sigma = sigma + diff/float(vega) # f(x) / f'(x)

    # value wasn't found, return best guess so far
    return sigma

def bs_price(cp_flag,S,K,T,r,v,q=0.0):
    
    N = norm.cdf
    d1 = (m.log(S/float(K))+(r+v*v/2.0)*T)/float(v*m.sqrt(T))
    d2 = d1-v*m.sqrt(T)
    if cp_flag == 'c':
        price = S*m.exp(-q*T)*N(d1)-K*m.exp(-r*T)*N(d2)
    else:
        price = K*m.exp(-r*T)*N(-d2)-S*m.exp(-q*T)*N(-d1)
    return price
	


def bs_vega(cp_flag,S,K,T,r,v,q=0.0):
    n = norm.pdf
    d1 = (m.log(S/float(K))+(r+v*v/2.)*T)/float(v*m.sqrt(T))
    return S * m.sqrt(T)*n(d1)

def black_scholes (cp, s, k, t, v, rf=0.0, div=0):
        """ Price an option using the Black-Scholes model.
        Input parameters
        s: initial stock price
        k: strike price
        t: expiration time
        v: volatility
        rf: risk-free rate
        div: dividend
        cp: +1/-1 for call/put
        
        :Return optprice: A float, option price
        """
        t = float(t)/365.0
        d1 = (m.log(s/k)+(rf-div+0.5*m.pow(v,2))*t)/(v*m.sqrt(t))
        d2 = d1 - v*m.sqrt(t)
        optprice = (cp*s*m.exp(-div*t)*stats.norm.cdf(cp*d1)) - (cp*k*m.exp(-rf*t)*stats.norm.cdf(cp*d2))
        return optprice
    
def calc_premium(underlyingValue,callPut,strikePrice,daysToExpiry,vix=0.15):
        """
        Input parameters
        :underlyingValue: A float, current price
        :callPut: A string, Call/Put
        :strikePrice: A float, strike
        :daysToExpiry: An int, days left to next expiry
    
        """    
        if callPut=='Call':            
            return black_scholes(1,underlyingValue,float(strikePrice),daysToExpiry,vix)  
        elif callPut=='Put':
            return black_scholes(-1,underlyingValue,float(strikePrice),daysToExpiry,vix)  
        else:
            print ('Enter Call or Put')
            return -1
        
def calc_vix(underlyingValue,callPut,strikePrice,daysToExpiry,price):
    """
    Input parameters
    :underlyingValue: A float, current price
    :callPut: A string, Call/Put
    :strikePrice: A string, strike
    :daysToExpiry: An int, days left to next expiry
    :price: A float,close price
    
    Output
    :Return i: A float
    """
    for j in range(1,1000):
        i= float(j)/1000    
        if callPut=='Call':
            curP =  black_scholes(1,underlyingValue,float(strikePrice),daysToExpiry,i)
        elif callPut=='Put':
            curP = black_scholes(-1,underlyingValue,float(strikePrice),daysToExpiry,i)  
        if curP>price:
            break
    return i

def option_delta(cp, s, k, t, v, rf=0.1, div=0):
        """ Price an option using the Black-Scholes model.
        Input parameters
        :s: initial stock price
        :k: strike price
        :t: expiration time
        :v: volatility
        :rf: risk-free rate
        :div: dividend
        :cp: +1/-1 for call/put
        """
        t = float(t)/365.0
        d1 = (m.log(s/k)+(rf-div+0.5*m.pow(v,2))*t)/(v*m.sqrt(t))
        delta = cp*m.exp(-div*t)*stats.norm.cdf(cp*d1)
        return delta


def calc_days_left_nifty(date1):
    """
    Input parameters
    :date1: A string,  'yyyy-mm-dd' format
    
    Output
    :Return ndays: An int, days left to next nifty Expiry
    """
    split_date = date1.split('-')
    if len(split_date)!=3:
        print ('Check date')
        return -1
    month = int(split_date[1])
    year = int(split_date[0])
    day = int(split_date[2])
    a,max_days = monthrange(year, month)
    for i in range(1,max_days+1): 
        weekday = date(year,month,i).weekday()
        if weekday==3:
            last_thurs = i
    ndays = (date(year,month,last_thurs)- date(year,month,day)).days
    if ndays<1:
        if month<12:
            month+=1
            a,max_days = monthrange(year, month)
            for i in range(1,max_days+1): 
                weekday = date(year,month,i).weekday()
                if weekday==3:
                    last_thurs = i
            ndays = (date(year,month,last_thurs)- date(year,month-1,day)).days
        else:
            month=1
            year+=1
            a,max_days = monthrange(year, month)
            for i in range(1,max_days+1): 
                weekday = date(year,month,i).weekday()
                if weekday==3:
                    last_thurs = i
            ndays = (date(year,month,last_thurs)- date(year-1,12,day)).days            
    return ndays



def get_option_code(OptionTicker, exch):
    """
    Input parameters
    :OptionTicker: Astring, trading symbol for the stock
    :exch: A string, "NFO"
    
    Output
    :Return OptioninstrumentCode: An int, stock option's instrument code
    """
    filename = exch + 'instrumentcode.xlsx'
    Sheet = "Sheet1"
    wb = xlrd.open_workbook(filename)
    s = wb.sheet_by_name(Sheet)
    row  = s.nrows
    for i in range(row - 1):
        if s.cell(i, 6).value == OptionTicker:
            OptioninstrumentCode = int(s.cell(i, 8).value)
            break
    return OptioninstrumentCode


def get_option_price(access_token,exch, ticker):
    """
    Input parameters
    :access_token: A string, Kite access token
    :exch: A string, "NFO"
    :ticker: A string
    
    Output
    :Return data: A dataframe
    """
    api_key = "c6y9qzf76nej1zbo"
    kite = KiteConnect(api_key=api_key)
    kite.set_access_token(access_token)
    price=0
    ntry=0
    while price==0  and ntry<5:
        ntry+=1
        try:
            data=kite.quote(exch+':'+str(ticker))[exch+':'+str(ticker)]
#            data = kite.quote(exch, ticker)
            price = (data['last_price'] + data['depth']['buy'][0]['price'] + data['depth']['sell'][0]['price'])/3.0
        except:
            print ('Trying to fetch Data')
            time.sleep(1)
    return data
    
def next_thursday():
    """
    Output
    :Return A datetime, next thursday's date 
    """
    today = datetime.now()
    return today + timedelta(days = 7 - (today.weekday() -3)%7)

def option_pnl(option_type, strike, quantity, price_option, price):
    """    
    Input parameters
    :option_type: A string, option type(CE/PE)
    :strike: A float
    :quantity: A float
    :price_option: A float
    :price: A flost

    Ouptut
    :Return pnl: A float, profit and loss value
    """
    if option_type=='PE':
        final_p = max(0,strike-price)
        pnl = quantity*(final_p - price_option)
    elif option_type =='CE':
        final_p = max(0,-strike+price)
        pnl = quantity*(final_p - price_option)
    return pnl


def get_pain(access_token, tradingsym, underlying, strike_dif):
    """    
    Input parameters
    :access_token: A string, Kite access token
    :tradingsym: A string, trading symbol for particular stock
    :underlying: A string, stock option's name
    :strike_dif: A float,, strike difference
    
    Output
    :Return A float, strike having minimum loss( i.e. maximum profit) 
            pain: A dataframe, complete data for all strikes values (i.e. 14) 
    
    """
    api_key = "c6y9qzf76nej1zbo"
    kite = KiteConnect(api_key=api_key)
    kite.set_access_token(access_token)
    exch="NFO"
    exch2 ="NSE"
    opt_maturity = datetime.now()
    #maturity = "17OCT"
    cur_month = datetime.strftime(opt_maturity, '%b').upper()
    cur_year = datetime.strftime(opt_maturity, "%y")
    
    maturity = str(cur_year)+cur_month
    length = 14
    columns = ['Strike', 'Price_PE', 'OI_PE', 'Price_CE', 'OI_CE'] 
    pain = pd.DataFrame(np.zeros((length, len(columns))),columns = columns)
    quoteinfy=kite.quote(exch2+':'+str(tradingsym))[exch2+':'+str(tradingsym)]
#    quoteinfy = kite.quote(exch2, tradingsym)
    price = quoteinfy["last_price"]
    strike = str(int(int(price / strike_dif)*strike_dif) - (length/2)*strike_dif)
    
    for i in range(length):
        pain['Strike'][i] = float(strike)+ i*strike_dif
        for option in ['CE', 'PE']:
            if underlying=='BANKNIFTY':
                ticker = underlying + str(next_thursday().day) + cur_month + cur_year +str(int(pain['Strike'][i])) + option
            else:
                ticker = underlying + maturity + str(int(pain['Strike'][i])) + option
            try:
                data = get_option_price(access_token, exch, ticker)
                pain['Price_'+option][i] = data['last_price']
                pain['OI_'+option][i] = data['open_interest']
            except:
                continue       
            time.sleep(1)
    
    pain['CE_pain'] = 0
    pain['PE_pain'] = 0
    
    for i in range(len(pain)):
        pnl_ce = 0
        pnl_pe = 0
        j=0
        for strike in list(pain['Strike']):
            pnl_ce += option_pnl('CE', strike, pain['OI_CE'][j]*0.01, pain['Price_CE'][j], pain['Strike'][i])
            pnl_pe += option_pnl('PE', strike, pain['OI_PE'][j]*0.01, pain['Price_PE'][j], pain['Strike'][i])
            j+=1
        pain['CE_pain'][i] = pnl_ce
        pain['PE_pain'][i] = pnl_pe
            
            
    pain['total_pain'] = pain['CE_pain'] + pain['PE_pain']
    return pain['Strike'][pain['total_pain'].idxmin()], pain

#get option_chain
def get_OI(access_token, tradingsym, underlying, strike_dif):
    """    
    Input parameters
    :access_token: A string, Kite access token
    :tradingsym: A string, trading symbol for particular stock
    :underlying: A string, stock option's name
    :strike_dif: A float,, strike difference
    
    Output
    :Return pain: A dataframe
    """
    api_key = "c6y9qzf76nej1zbo"
    kite = KiteConnect(api_key=api_key)
    kite.set_access_token(access_token)
    exch="NFO"
    exch2 ="NSE"
    opt_maturity = datetime.now()
    #maturity = "17OCT"
    cur_month = datetime.strftime(opt_maturity, '%b').upper()
    cur_year = datetime.strftime(opt_maturity, "%y")
    
    maturity = str(cur_year)+cur_month
    length = 14
    columns = ['Strike', 'Price_PE', 'OI_PE', 'Price_CE', 'OI_CE'] 
    pain = pd.DataFrame(np.zeros((length, len(columns))),columns = columns)
    quoteinfy=kite.quote(exch2+':'+str(tradingsym))[exch2+':'+str(tradingsym)]
#    quoteinfy = kite.quote(exch2, tradingsym)
    price = quoteinfy["last_price"]
    strike = str(int(int(price / strike_dif)*strike_dif) - (length/2)*strike_dif)
    
    for i in range(length):
        pain['Strike'][i] = int(strike)+ i*strike_dif
        for option in ['CE', 'PE']:
            if underlying=='BANKNIFTY':
                ticker = underlying + str(next_thursday().day) + cur_month + cur_year +str(int(pain['Strike'][i])) + option
            else:
                ticker = underlying + maturity + str(int(pain['Strike'][i])) + option
            try:
                data = get_option_price(access_token, exch, ticker)
                pain['Price_'+option][i] = data['last_price']
                pain['OI_'+option][i] = data['open_interest']
            except:
                continue       
            time.sleep(1)
    return pain

def import_nsepydata(sym, startD,endD,fut,expiry):
    """    
    Input parameters
    :sym: A string, stock option's name
    :startD: A datetime, start date
    :endD: A datetime, end date
    :fut: A boolean value: True(futures)/ False(Cash)
    :expiry: A datetime, next expiry date
    
    Output
    :Return stock: A dataframe, complete data using nsepy.get_hisory
    """
    if(fut==True):
        stock = nsepy.get_history(symbol=sym,start=startD, end=endD,index=True,futures=fut,expiry_date=expiry)
    else:
        stock = nsepy.get_history(symbol=sym,start=startD, end=endD,index=True,futures=fut)
    return stock

def get_vol(access_token, underlying, strike, option, price, opt_maturity):
    """
    ###Calculate volatility using kite###
    
    Input parameters
    :access_token: A string, Kite access token
    :underlying: A string, stock option's name
    :strike: A string
    :option: A string, option type-CE/PE
    :price: A float, close price
    :opt_maturity: A datetime, next expiry date
    
    Output
    :Return vol: A float, volatility 
    """
    api_key = "c6y9qzf76nej1zbo"
    kite = KiteConnect(api_key=api_key)
    kite.set_access_token(access_token)
    exch = 'NFO'
    cur_month = datetime.strftime(opt_maturity, '%b').upper()
    cur_year = datetime.strftime(opt_maturity, "%y")
    maturity = str(cur_year)+cur_month
    ticker = underlying+maturity+strike+option
    quote = get_option_price(access_token,exch, ticker)['last_price']
    today = datetime.now()
    difference_in_seconds = abs(mktime(opt_maturity.timetuple()) - mktime(today.timetuple()))
    years = difference_in_seconds/float(60*60*24*365)
    if option =='CE':
        vol = find_vol(quote, "c", price, int(strike), years, 0.1)
    elif option =='PE':
        vol = find_vol(quote, "p", price, int(strike), years, 0.1)
    return vol

def get_vol_nsepy(date1,underlying, strike, option, price, opt_maturity):
    """
    ###Calculate volatility using nsepy###
    
    Input parameters
    :date1:A datetime, current date who's data has to be calculated
    :underlying: A string, stock option's name
    :strike: A string
    :option: A string, option type-CE/PE
    :price: A float, future data close price
    :opt_maturity: A datetime, next expiry  
    
    Output
    :Return vol: A float, volatility    
    """
    cur_month = datetime.strftime(opt_maturity, '%b').upper()
    cur_year = datetime.strftime(opt_maturity, "%y")
    maturity = str(cur_year)+cur_month
    ticker = underlying+maturity+strike+option
    if(underlying=='NIFTY'or underlying=='BANKNIFTY'):
        isIndex=True
    else:
        isIndex=False
    quote = get_price(ticker,date1,underlying,isIndex)
    if(quote['Close'][0]<=(price-float(strike)) and option=='CE'):
        ticker = underlying+maturity+strike+'PE'
        quote = get_price(ticker,date1,underlying,isIndex)
        quote['Close'][0]+= price-float(strike)
        print(quote['Close'][0])
    if(quote['Close'][0]<=(float(strike)-price) and option=='PE'):
        ticker = underlying+maturity+strike+'CE'
        quote = get_price(ticker,date1,underlying,isIndex)
        quote['Close'][0]+= float(strike)-price
        print(quote['Close'][0])

    difference_in_seconds = abs(mktime(opt_maturity.timetuple()) - mktime(date1.timetuple()))
    years = difference_in_seconds/float(60*60*24*365)
    if option =='CE':
        vol=calc_vix(price, 'Call',int(strike), (years*365),quote['Close'][0])
#        vol = find_vol(quote['Close'][0], "c", price, int(strike), years, 0.1)
    elif option =='PE':
        vol=calc_vix(price, 'Put',int(strike), (years*365),quote['Close'][0])
#        vol = find_vol(quote['Close'][0], "p", price, int(strike), years, 0.1)
    return vol

#def get_expiry(date1):
#    input_n = date1
#    a,max_days = monthrange(date1.year, date1.month)
#    while(max_days>20):
#        date1=date1.replace(day=max_days)
#        if(date1.weekday()==3):
#            ans = date1.replace(hour=15, minute = 30 )
#            if ans>input_n:
#                return ans
#            else:
#                return get_expiry(datetime(input_n.year + int(input_n.month / 12), ((input_n.month % 12) + 1), 1))
#        else:
#            max_days-=1

def get_strike_diff(underlying):
    """
    Input parameter
    :underlying: A string, stock option's name
    
    Ouput
    :Return : A float, strike difference
    """
    
    if underlying =='NIFTY' or underlying =='BANKNIFTY':
        return 100.0
    data = pd.read_excel('strike_price.xlsx')
    return float(data[data['Ticker ']==underlying]['Strike  Diff.'])

def get_price(ticker1, fetch_date, underlying,isIndex):
    """
    Input parameters
    :ticker1: A string, ticker for paricular stock
    :fetch_date: A datetime, current date who's data has to be calculated
    :underlying: A string, stock option's name
    :isIndex: A boolean value, True : NIFTY/BANKNIFTY 
                               False: Others   
    Ouptut                                 
    :Return data: A dataframe, complete data for that particular ticker                                     
    """
    price=0
    ntry=0
    r = re.compile("([a-zA-Z]*\-?\&?[a-zA-Z]+)([0-9]+)([a-zA-Z]+)([0-9]*\.?[0-9]+)([a-zA-Z]+)")  
    ticker=r.match(ticker1).groups()
    while price==0  and ntry<5:
        ntry+=1
        try:
            if isIndex ==True:
                data = nsepy.get_history(symbol=ticker[0], start=fetch_date, end=fetch_date,option_type=ticker[4], strike_price=float(ticker[3]),expiry_date=calc_expiry(fetch_date), index=True)
            else:
                data = nsepy.get_history(symbol=ticker[0], start=fetch_date, end=fetch_date,option_type=ticker[4], strike_price=float(ticker[3]),expiry_date=calc_expiry(fetch_date))
            price = data['Close'].iloc[0]
        except Exception as e:
            print ('Trying to fetch Data',ticker1, e.__class__.__name__)
            time.sleep(1)
    return data

def calc_expiry(crdate,value='next'):
    """
    Input parameters
    :crdate: A datetime, date who's expiry has to be calculated
    :value: A string, by default 'next' - to calculate the next expiry date
                                 'prev' - to calculate the previous expiry date
    Output
    :Return crdate: A datetime, expiry date 
    """
    expiry_list=pd.read_csv("expiry_list.csv")

    if(value=='next'):
        crdate+=timedelta(days=1)
        if(crdate.weekday()>4):
            crdate+=timedelta(days=7-crdate.weekday())
        while(int(expiry_list[expiry_list['Date']==str(crdate.strftime('%Y-%m-%d'))]['Expiry'].iloc[0]) !=1):
            if(crdate.weekday()>=4):
                crdate+=timedelta(days=7-crdate.weekday())
            else:    
                crdate+=timedelta(days=1)
    elif(value=='prev'):
#        crdate-=timedelta(days=1)
        if(crdate.weekday()>4):
            crdate-=timedelta(days=crdate.weekday()-4)
        while(int(expiry_list[expiry_list['Date']==str(crdate.strftime('%Y-%m-%d'))]['Expiry'].iloc[0]) !=1):
            if(crdate.weekday()>4):
                crdate-=timedelta(days=crdate.weekday()-4)
            else:
                crdate-=timedelta(days=1)    
    return crdate
            
def get_pain_anyDate(underlying,fetch_date):
    """
    Input parameters
    :underlying: A string, stock option's name
    :fetch_date: A datetime, date who's data has to be calculated
    
    Output
    :Return A float, strike having minimum loss( i.e. maximum profit) 
            pain: A dataframe, complete data for all strikes values (i.e. 14) 
    
    """
    opt_maturity = calc_expiry(fetch_date)
    if underlying not in ['NIFTY', 'BANKNIFTY']:
        isIndex = False
    else:
        isIndex = True
    cur_month = datetime.strftime(opt_maturity, '%b').upper()
    cur_year = datetime.strftime(opt_maturity, "%y")

    maturity = str(cur_year)+cur_month
    length = 14
    columns = ['Strike', 'Price_PE', 'OI_PE', 'Price_CE', 'OI_CE'] 
    pain = pd.DataFrame(np.zeros((length, len(columns))),columns = columns)
    if isIndex ==True:
        quote =  nsepy.get_history(symbol=underlying, start=fetch_date, end=fetch_date, futures=True, expiry_date=calc_expiry(fetch_date), index = True)
    else:
        quote =  nsepy.get_history(symbol=underlying, start=fetch_date, end=fetch_date, futures=True, expiry_date=calc_expiry(fetch_date))
    price = quote["Close"].iloc[0]
    strike_dif = int(get_strike_diff(underlying))
    strike = str(int(int(price / strike_dif)*strike_dif) - (length/2)*strike_dif)
    
    for i in range(length):
        pain['Strike'][i] = float(strike)+ i*strike_dif
        for option in ['CE', 'PE']:
            ticker = underlying + maturity + str(int(pain['Strike'][i])) + option
            try:
                data = get_price(ticker, fetch_date, underlying,isIndex)
                pain['Price_'+option][i] = data['Close']
                pain['OI_'+option][i] = data['Open Interest']
            except:
                pain['Price_'+option][i] = 0
                pain['OI_'+option][i] = 0
            time.sleep(1)
    
    pain['CE_pain'] = 0
    pain['PE_pain'] = 0
    
    for i in range(len(pain)):
        pnl_ce = 0
        pnl_pe = 0
        j=0
        for strike in list(pain['Strike']):
            pnl_ce += option_pnl('CE', strike, pain['OI_CE'][j]*0.01, pain['Price_CE'][j], pain['Strike'][i])
            pnl_pe += option_pnl('PE', strike, pain['OI_PE'][j]*0.01, pain['Price_PE'][j], pain['Strike'][i])
            j+=1
        pain['CE_pain'][i] = pnl_ce
        pain['PE_pain'][i] = pnl_pe
            
            
    pain['total_pain'] = pain['CE_pain'] + pain['PE_pain']
    return pain['Strike'][pain['total_pain'].idxmin()], pain
    

def get_sector(underlying):
    data = pd.read_excel('sector.xlsx')
    try:
        return str(data[data['Tickers']==underlying]['Peers'].iloc[0])
    except:
        return ""
        
def is_connected():
    try:
        # connect to the host -- tells us if the host is actually
        # reachable
        socket.create_connection(("www.google.com", 80))
        return True
    except OSError:
        pass
    return False

def calc_strategy_params(pnl_list,base_price=1,time_period = 'yearly'):
    
    #total profit, average profit
    total_profit = sum(pnl_list)
    print 'Total Profits are:', total_profit
    avg_profit = total_profit/float(len(pnl_list))
    print 'Average Profit:', avg_profit
    # Winning Percentage, Lossing Percentage
    win_perc = sum(i > 0 for i in pnl_list)/ float(len(pnl_list))
    loss_perc = sum(i < 0 for i in pnl_list)/ float(len(pnl_list))
    print 'Win Percentage:', win_perc
    print 'Loss Percentage:', loss_perc
    
    # Calculate Max Drawdown
    max_drawdown = 0
    temp_sum= 0
    for i in range(len(pnl_list)):
        if temp_sum+pnl_list[i]<0:
            temp_sum=temp_sum + pnl_list[i]
        else:
            temp_sum = 0
        if temp_sum<max_drawdown:
            max_drawdown =temp_sum
        
    print 'Max Drawdown:', max_drawdown
    print 'Max Drawdown wrt to Average Profit:', abs(max_drawdown/avg_profit)
    
    #profit Factor
    profit_only = [i for i in pnl_list if i >= 0]
    loss_only = [i for i in pnl_list if i < 0]
    print 'Average Profit:', sum(profit_only)/len(profit_only)
    print 'Average Loss:', sum(loss_only)/len(loss_only)
    profit_factor = abs(sum(profit_only)/float(sum(loss_only)))
    print 'Profit Factor:', profit_factor
    
    #Sharpe Ratio
    risk_free_ratio = 7
    if time_period == 'monthly':
        scaling_factor = 12.0
    elif time_period == 'daily':
        scaling_factor =255.0
    else:
        scaling_factor = 1.0
    std_error = np.std(pnl_list)/pow(len(pnl_list),0.5)
    sharpe_ratio = np.sqrt(scaling_factor)*((avg_profit/base_price)*100 - risk_free_ratio/scaling_factor)/np.std([x*100 / base_price for x in pnl_list])
    print 'Standard Error:', std_error
    print 'Sharpe Ratio:',sharpe_ratio
    
    #K-Ratio, vami
    vami = [base_price]
    logvami  = [np.log10(vami[0])]
    for i in pnl_list:
        vami.append(vami[-1]*(1+i/base_price))
        logvami.append(np.log10(vami[-1]))
#    m, b, r_value, p_value, std_err = sp.stats.linregress(range(len(pnl_list)+1), logvami)
#    if std_err>0:
#        kRatio = m/std_err
#    else:
#        kRatio = 'Capital Lost'
#    print 'K-Ratio:', kRatio
    kRatio = 0
    #Did the strategy pass?    
    strategyPass = False
    if total_profit>0 and abs(max_drawdown/avg_profit)<2.5 and profit_factor>1.5 and sharpe_ratio>1.0 and kRatio>1.5:
        strategyPass = True
    return strategyPass
