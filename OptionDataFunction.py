'''
Fill the missing value in the Option database created using OptionDataTOdb
using black scholes
Use fetch data funstion to fetch the value of any particular ticker
'''


import pandas
import sqlite3
import re
import datetime
from monthdelta import monthdelta
import math as m
from scipy import stats
from time import strptime
from calendar import monthrange
from utilities import calc_expiry

# ==DATABASE===================================================================

conn = sqlite3.connect('C:/Users/DELL/.spyder-py3/OPTtest.db')
conn_future=sqlite3.connect('C:/Users/DELL/Commodity_OptionData/OPTFUT.db')
conn2 = sqlite3.connect('OPTtest2.db')
cursor = conn.execute("SELECT Symbol, Expiry_Month, Expiry_Year, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt, IV, UnderlyingPrice, FilledData from OPTION ")
cursor_future = conn_future.execute("SELECT Symbol, Expiry_Month, Expiry_Year, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt, IV, UnderlyingPrice, FilledData from OPTION ")
try:
    cursor2 = conn2.execute("SELECT Symbol, Expiry_Month, Expiry_Year, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt, IV, UnderlyingPrice, FilledData from OPTION2")
    cursor2.execute('drop table if exists OPTION2')
except:
    pass

conn2.execute('''CREATE TABLE OPTION2
          (Symbol           TEXT    NOT NULL,
          Expiry_Year     TEXT    NOT NULL,
          Expiry_Month      TEXT    NOT NULL,
          Strike            INT    NOT NULL,  
          OptionType      TEXT    NOT NULL,
          Date            TEXT     NOT NULL,
          Time           TEXT  NOT NULL,
          Open            FLOAT     NOT NULL,
          High           FLOAT     NOT NULL,
          Low           FLOAT     NOT NULL,
          Close            FLOAT     NOT NULL,
          Volume           FLOAT     NOT NULL,
          OpenInt          FLOAT       NOT NULL,
          IV               FLOAT     NOT NULL, 
          UnderlyingPrice  FLOAT     NOT NULL,
          FilledData       TEXT     NOT NULL);''')
cursor2 = conn2.execute("SELECT Symbol, Expiry_Month, Expiry_Year, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt, IV, UnderlyingPrice, FilledData from OPTION2")
# =============================================================================

cursor.execute("SELECT DISTINCT Date FROM OPTION")
d=cursor.fetchall()

date_list=[] 
for z in range(0,len(d)):
    date_list.append(d[z][0])
                    
    
def Missing_timestamp(op,date,symbol,exmonth,strike,flag,exyear):
    tm=datetime.datetime(date.year,date.month,date.day,9,15,59)
    while(str(tm.time())<='15:30:59'):
        cursor2.execute("SELECT Symbol FROM OPTION2 WHERE Symbol = ? AND Expiry_Month = ? AND Strike = ? AND Time = ? AND OptionType = ? AND Date = ? ", (symbol,exmonth,strike,str(tm.time()),op,str(tm.date())))
        data=cursor2.fetchall()  
                    
        if not data:
            if(str(tm.time())=='09:15:59'):
                t=tm                            
                t=tm+datetime.timedelta(days=-1)
                while((str(t.date()))>'2016-01-01'):
                    if (str(t.date())) in date_list:
                        break
                    else:
                        t=t+datetime.timedelta(days=-1)  
                if(flag==0):
                    if(op=="FUT"):
                        cursor_future.execute("SELECT * FROM OPTION WHERE Symbol = ? AND Expiry_Month = ? AND Strike = ? AND OptionType = ? AND Date = ? AND Expiry_Year = ? ORDER BY Time DESC", (symbol,exmonth,strike,op,str(t.date()),exyear))
                        data=cursor_future.fetchall()
                    else:   
                        cursor.execute("SELECT * FROM OPTION WHERE Symbol = ? AND Expiry_Month = ? AND Strike = ? AND OptionType = ? AND Date = ? AND Expiry_Year = ? ORDER BY Time DESC", (symbol,exmonth,strike,op,str(t.date()),exyear))
                        data=cursor.fetchall()                
                else:    
                    cursor2.execute("SELECT * FROM OPTION2 WHERE Symbol = ? AND Expiry_Month = ? AND Strike = ? AND Time = ? AND OptionType = ? AND Date = ?", (symbol,exmonth,strike,'15:30:59',op,str(t.date())))
                    data=cursor2.fetchall()
                        
                if(tm.month>strptime(exmonth,'%b').tm_mon):
                    exp_yr=tm.year+1
                else:
                    exp_yr=tm.year
                if not data:     
                    cursor2.execute("INSERT INTO OPTION2 (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(symbol, exp_yr, exmonth, strike, op, str(tm.date()), str(tm.time()), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 'YES')); 
                    conn2.commit()               
                else:
                    cursor2.execute("INSERT INTO OPTION2 (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt, IV, UnderlyingPrice, FilledData) \
                                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(data[0][0], exp_yr, exmonth, data[0][3], data[0][4], str(tm.date()), str(tm.time()), data[0][7], data[0][8], data[0][9], data[0][10], 0.0, 0.0, 0.0, 0.0, 'YES')); 
                    conn2.commit() 
            else:
                cursor2.execute("SELECT * FROM OPTION2 WHERE Symbol = ? AND Expiry_Month = ? AND Strike = ? AND Time = ? AND OptionType = ? AND Date = ?", (symbol,exmonth,strike,str((tm - datetime.timedelta(hours=1/60)).time()),op,str(tm.date())))
                data=cursor2.fetchall()  
                cursor2.execute("INSERT INTO OPTION2 (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                                               VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(data[0][0], data[0][1], data[0][2], data[0][3], data[0][4], str(tm.date()), str(tm.time()), data[0][7], data[0][8], data[0][9], data[0][10], 0.0, 0.0, 0.0, 0.0, 'YES')); 
                conn2.commit()               
        tm = tm + datetime.timedelta(hours=1/60)
                    
def update(op,date,symbol,exyear,exmonth,strike):
    
    tm=datetime.datetime(date.year,date.month,date.day,9,15,59)
    tdf=375
    while(str(tm.time())<='15:30:59'):
        cursor2.execute("SELECT Close FROM OPTION2 WHERE Symbol = ? AND Expiry_Month = ? AND Time = ? AND OptionType = ? AND Date = ?",(symbol,(calc_expiry(tm).strftime("%b").upper()),str(tm.time()),'FUT',str(tm.date())))
        data=cursor2.fetchall()   
        cursor2.execute("SELECT Close FROM OPTION2 WHERE Symbol = ? AND Expiry_Month = ? AND Time = ? AND OptionType = ? AND Date = ? AND Strike = ? AND Expiry_Year = ?",(symbol,exmonth,str(tm.time()),op,str(tm.date()),strike,exyear))
        close=cursor2.fetchall()
        cursor2.execute('''UPDATE OPTION2 SET IV = ?, UnderlyingPrice =? WHERE Symbol = ? AND Expiry_Month = ? AND Strike = ? AND Time = ? AND OptionType = ? AND Date = ? ''',(calc_vix(data[0][0],op,strike,(calc_days_left(tm,exmonth)+float(tdf/375)),close[0][0]), data[0][0], symbol,exmonth,strike,str(tm.time()),op,str(tm.date())))
        conn2.commit()
        tdf-=1
        tm = tm + datetime.timedelta(hours=1/60)

def black_scholes (cp, s, k, t, v, rf=0.010, div=0):
        """ Price an option using the Black-Scholes model.
        s: initial stock price
        k: strike price
        t: expiration time
        v: volatility
        rf: risk-free rate
        div: dividend
        cp: +1/-1 for call/put
        """
        t = float(t)/365.0
        d1 = (m.log(s/k)+(rf-div+0.5*m.pow(v,2))*t)/(v*m.sqrt(t))
        d2 = d1 - v*m.sqrt(t)
        optprice = (cp*s*m.exp(-div*t)*stats.norm.cdf(cp*d1)) - (cp*k*m.exp(-rf*t)*stats.norm.cdf(cp*d2))
        return optprice
    
def calc_days_left(d,search_month):
    """
    Input date: 'yyyy-mm-dd'
    return days left to next nifty Expiry
    """
    nd=0
    if (search_month!=d.strftime("%b")):
        while(search_month!=d.strftime("%b").upper()):
            a,max_days = monthrange(d.year, d.month)
            nd+=max_days
            d=d+monthdelta(1)
            
    date=str(d.date())    
    split_date = date.split('-')
    if len(split_date)!=3:
        print ('Check date')
        return -1
    month = int(split_date[1])
    year = int(split_date[0])
    day = int(split_date[2])
    a,max_days = monthrange(year, month)
    for i in range(1,max_days+1): 
        weekday = datetime.date(year,month,i).weekday()
        if weekday==3:
            last_thurs = i
    ndays = (datetime.date(year,month,last_thurs)- datetime.date(year,month,day)).days
    if ndays<1:
        if month<12:
            month+=1
            a,max_days = monthrange(year, month)
            for i in range(1,max_days+1): 
                weekday = datetime.date(year,month,i).weekday()
                if weekday==3:
                    last_thurs = i
            ndays = (datetime.date(year,month,last_thurs)- datetime.date(year,month-1,day)).days
        else:
            month=1
            year+=1
            a,max_days = monthrange(year, month)
            for i in range(1,max_days+1): 
                weekday = datetime.date(year,month,i).weekday()
                if weekday==3:
                    last_thurs = i
            ndays = (datetime.date(year,month,last_thurs)- datetime.date(year-1,12,day)).days 

    return ndays+nd

def calc_vix(underlyingValue, callPut, strikePrice, daysToExpiry, price):
    for j in range(1,100):
        i= float(j)/100      
        if callPut=='CALL':
            curP =  black_scholes(1,underlyingValue,float(strikePrice),daysToExpiry,i)
        elif callPut=='PUT':
            curP = black_scholes(-1,underlyingValue,float(strikePrice),daysToExpiry,i)  
        if curP>price:
            break
    return i


def fetch_data(ticker, from_date, to_date):
    flag=0
    if(ticker[-3:]=='FUT'):
        r = re.compile("([TUF]+)([a-zA-Z]+)([0-9]+)(\w+)")
        tic=ticker[::-1]
        a=r.match(tic).groups()
        op, exmonth, exyear, symbol, strike=a[0][::-1], a[1][::-1], 2000+int(a[2][::-1]), a[3][::-1], 0.0
    elif(ticker[-2:]=='CE' or ticker[-2:]=='PE'):
        r = re.compile("([a-zA-Z]+)([0-9]*\.?[0-9]+)([a-zA-Z]+)([0-9]+)(\w+)")
        tic=ticker[::-1]
        a=r.match(tic).groups()
        if(a[0][::-1]=='PE'):
            op='PUT'
        elif(a[0][::-1]=='CE'):
            op='CALL'
        if(a[4][::-1]=='BANKNIFTY'):                                                       
            symbol, exyear, exmonth, strike = a[4][::-1], 2000+int(a[1][-2:][::-1]), a[3][::-1]+a[2][::-1],a[1][0:-2][::-1]                                                                                  
        else:
            symbol, exyear, exmonth, strike=a[4][::-1],2000+int(a[3][::-1]),a[2][::-1],a[1][::-1]
    else:
        print("Invalid Ticker")
        
                                
    from_date=datetime.datetime.strptime(from_date, "%Y-%m-%d")
    to_date=datetime.datetime.strptime(to_date, "%Y-%m-%d")
    if(from_date.weekday()>=5):
            from_date+=datetime.timedelta(days=(7-from_date.weekday()))
    while (from_date<=to_date):
        print(str(from_date.date()))
        if(op=="FUT"):
            cursor_future.execute("SELECT * FROM OPTION WHERE Symbol = ? AND Expiry_Year = ? AND Expiry_Month = ? AND Strike = ? AND Date = ? AND OptionType = ?",(symbol,exyear,exmonth,strike,str(from_date.date()),op))
            data=cursor_future.fetchall()

#            cursor.execute("SELECT * FROM OPTION WHERE Symbol = ? AND Expiry_Year = ? AND Expiry_Month = ? AND Strike = ? AND Date = ? AND OptionType = ?",(symbol,exyear,exmonth.capitalize(),strike,str(from_date.date()),op))
#            data=cursor.fetchall()
            if data:
                for q in range(0,len(data)):
                    cursor2.execute("INSERT INTO OPTION2 (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(data[q][0],data[q][1],data[q][2].upper(),data[q][3],data[q][4],data[q][5],data[q][6],data[q][7],data[q][8],data[q][9],data[q][10],data[q][11],data[q][12],data[q][13],data[q][14],data[q][15])); 
                conn2.commit()
                Missing_timestamp(op,from_date,symbol,exmonth,strike,flag,exyear)
                flag=1
            else:
                print("Data for Date:",str(from_date.date()),"is not available")
               
        elif(op=="PUT" or op=="CALL"):
            cursor.execute("SELECT * FROM OPTION WHERE Symbol = ? AND Expiry_Year = ? AND Expiry_Month = ? AND Strike = ? AND Date = ? AND OptionType = ?",(symbol,exyear,exmonth,strike,str(from_date.date()),op))
            data=cursor.fetchall()
            if data:
                for q in range(0,len(data)):
                    cursor2.execute("INSERT INTO OPTION2 (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(data[q][0],data[q][1],data[q][2],data[q][3],data[q][4],data[q][5],data[q][6],data[q][7],data[q][8],data[q][9],data[q][10],data[q][11],data[q][12],data[q][13],data[q][14],data[q][15])); 
                conn2.commit()                
        
#                t=from_date+monthdelta(months=1)
                t=calc_expiry(from_date)
                cursor_future.execute("SELECT * FROM OPTION WHERE Symbol = ? AND Expiry_Year = ? AND Expiry_Month = ? AND Strike = ? AND Date = ? AND OptionType = ?",(symbol,str(t.year),t.strftime("%b").upper(),0.0,str(from_date.date()),'FUT'))
#                cursor.execute("SELECT * FROM OPTION WHERE Symbol = ? AND Expiry_Year = ? AND Expiry_Month = ? AND Strike = ? AND Date = ? AND OptionType = ?",(symbol,str(t.year),t.strftime("%b"),0.0,str(from_date.date()),'FUT'))                
                data=cursor_future.fetchall()
#                data=cursor.fetchall()
                
                if data:
                    for q in range(0,len(data)):
                        cursor2.execute("INSERT INTO OPTION2 (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(data[q][0],data[q][1],data[q][2],data[q][3],data[q][4],data[q][5],data[q][6],data[q][7],data[q][8],data[q][9],data[q][10],data[q][11],data[q][12],data[q][13],data[q][14],data[q][15])); 
                    conn2.commit()                
        
                    Missing_timestamp(op,from_date,symbol,exmonth,strike,flag,exyear)
                    Missing_timestamp('FUT',from_date,symbol,t.strftime("%b").upper(),0.0,flag,exyear)
                    update(op,from_date,symbol,exyear,exmonth,strike)
                    flag=1
                else:
                    print(" Future Data for Date:",str(from_date.date()),"is not available")
        
            else:
                print("Data for Date:",str(from_date.date()),"is not available")
        else:
            print("Invalid Ticker")
            break
        from_date+=datetime.timedelta(days=1)
        if(from_date.weekday()>=5):
            from_date+=datetime.timedelta(days=(7-from_date.weekday()))
    finaldata=cursor2.execute("SELECT * from OPTION2  WHERE OptionType = ? ORDER BY Symbol,Date,OptionType,Strike,Time ASC",(op,))
    df = pandas.DataFrame(finaldata.fetchall(),columns=('Symbol', 'Expiry_Year', 'Expiry_Month', 'Strike', 'OptionType', 'Date', 'Time', 'Open', 'High', 'Low', 'Close', 'Volume', 'OpenInt', 'IV', 'UnderlyingPrice', 'FilledData'),index=None)
    return df
        
from_date="2016-01-12"
to_date="2016-01-12" 
ticker='ABIRLANUVO16JAN2200PE'
#ticker='RELIANCE16AUGFUT'
#from_date="2016-01-29"
#to_date="2016-01-31"   
df1=fetch_data(ticker,from_date,to_date)                        

conn.close()
conn2.close()
conn_future.close()
