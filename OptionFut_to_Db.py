'''
Converts OptionData .csv files into a database ( ONLY FUTURE, no CALL/PUT)

'''
import pandas
import sqlite3
from datetime import datetime,timedelta
from utilities import calc_expiry
import os
file_list=os.listdir("C:\\Users\\DELL\\.spyder-py3\\imba files\\IEOD DATA FNO SETP 2014 TO DEC 2015")

# ==DATABASE===================================================================
conn = sqlite3.connect('OPTFUT.db')
try:
    conn.execute('''CREATE TABLE OPTION
              (Symbol           TEXT    NOT NULL,
              Expiry_Year     TEXT    NOT NULL,
              Expiry_Month      TEXT    NOT NULL,
              Strike            INT    NOT NULL,  
              OptionType      TEXT    NOT NULL,
              Date            TEXT     NOT NULL,
              Time           TEXT  NOT NULL,
              Open            FLOAT     NOT NULL,
              High           FLOAT     NOT NULL,
              Low           FLOAT     NOT NULL,
              Close            FLOAT     NOT NULL,
              Volume           FLOAT     NOT NULL,
              OpenInt          FLOAT       NOT NULL,
              IV               FLOAT     NOT NULL, 
              UnderlyingPrice  FLOAT     NOT NULL,
              FilledData       TEXT     NOT NULL);''')
except:
    pass

cursor = conn.execute("SELECT Symbol, Expiry_Month, Expiry_Year, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt, IV, UnderlyingPrice, FilledData from OPTION")

# =============================================================================
for k in range(237,len(file_list)):
    print(k)
    io = pandas.read_csv('C:/Users/DELL/.spyder-py3/imba files/IEOD DATA FNO SETP 2014 TO DEC 2015/'+ file_list[k] +'')
    cdate=datetime.strptime(io['Date'][0], "%d/%m/%Y")
    dt1=calc_expiry(cdate)
    dt2=calc_expiry(dt1+timedelta(days=1))
    dt3=calc_expiry(dt2+timedelta(days=1))
    for i in range(len(io)):
        if(io['Ticker'][i][-2:]=='-I' or io['Ticker'][i][-3:]=='-II' or io['Ticker'][i][-4:]=='-III'):
            
            date1=datetime.strptime(io['Date'][i], "%d/%m/%Y").date()
            time=datetime.strptime(io['Time'][i],"%H:%M:%S").time()
            dt=datetime.combine(date1,time)
            if(io['Ticker'][i][-2:]=='-I'):
                dt=dt1
                sym=io['Ticker'][i][0:-2]
            elif(io['Ticker'][i][-3:]=='-II'):
                sym=io['Ticker'][i][0:-3]
                dt=dt2
            elif (io['Ticker'][i][-4:]=='-III'):
                sym=io['Ticker'][i][0:-4]
                dt=dt3
            else:
                print (i, io['Ticker'][i][-3:])
            conn.execute("INSERT INTO OPTION (Symbol, Expiry_Year, Expiry_Month, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt,IV, UnderlyingPrice, FilledData) \
                         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(str(sym), dt.year, dt.strftime("%b").upper(), 0, 'FUT', str(date1), str(time), str(io['Open'][i]), str(io['High'][i]), str(io['Low'][i]), str(io['Close'][i]), str(io['Volume'][i]), str(io['Open Interest'][i]), 0.0, 0.0, 'NO')); 
    
        else:
            continue

conn.commit()

#cursor = conn.execute("SELECT Symbol, Expiry_Month, Expiry_Year, Strike, OptionType, Date, Time, Open, High, Low, Close, Volume, OpenInt, IV, UnderlyingPrice, FilledData from OPTION")

#cursor.execute("SELECT DISTINCT Symbol FROM OPTION")
#data=cursor.fetchall()

conn.close()
