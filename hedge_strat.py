# -*- coding: utf-8 -*-
"""
Created on Fri Apr 06 12:35:46 2018

@author: abc
"""

from utilities import calc_expiry
from datetime import datetime, timedelta
#import numpy as np
#from monthdelta import monthdelta
import nsepy
import matplotlib.pyplot as plt
import pandas as pd
#import scipy.optimize as spo
#import numpy as np
a  = lambda x: round(x,-2) if round(x,-2)<x else round(x,-2) - 100

startD = datetime(2013, 1, 1)
endD = datetime.now()
nifty = nsepy.get_history("NIFTY", startD, endD, index= True)
pnl = pd.DataFrame(columns = ['Date', 'thresh', 'thresh2',  'optionType', 'price', 'type', 'buyP', 'sellP', 'pnl', 'cur_strike','hedgepnl', 'buyh', 'sellh'])
#pnl = pd.read_csv('hedge_strat.csv')
for optionType in ['CE']:
    for thresh_i in range(2,4):
        for thresh2_i in range(2,4): 
            fetchD = startD
            expD = prev_expD = startD.date()
            expD_2 = prev_expD_2 = startD
            prev_expD_2 = prev_expD= nifty.index[0]
            thresh =  thresh_i*100
            thresh2= thresh2_i*100
            cur_strike = nifty.iloc[0]['Close']
            print thresh_i, thresh2_i
            for i in nifty.index:
                fetchD = i    
                expD_2 =  calc_expiry(fetchD)
                expD = calc_expiry(fetchD)
                if expD>endD.date():
                    break
                try: 
                    if optionType== 'PE':
                        getATMstrike = a(nifty.loc[i, 'Close'])
                        hedgestrike = getATMstrike - thresh
                    else:
                        getATMstrike = a(nifty.loc[i, 'Close']) + 100
                        hedgestrike = getATMstrike + thresh

                except:
                    getATMstrike = a(nifty.loc[i, 'Close'])
                if getATMstrike - thresh2> cur_strike: 
                    putP_atm = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = getATMstrike, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
                    sell_put = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = cur_strike, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
                    buy_putP = putP_atm.iloc[0]
                    try:
                        sell_putP = sell_put.iloc[0]
                    except:
                        sell_putP = 0
                    putP_h = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = hedgestrike, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
                    sellh_put = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = cur_strike+ thresh, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
                    buyh_putP = putP_h.iloc[0]
                    try:
                        sellh_putP = sellh_put.iloc[0]
                    except:
                        sellh_putP = 0
                    cur_strike = getATMstrike
                    pnl.loc[len(pnl)] = [fetchD, thresh,thresh2,  optionType, nifty.loc[i, 'Close'], 'shift up', buy_putP, sell_putP, sell_putP - buy_putP, cur_strike, buyh_putP - sellh_putP, buyh_putP, sellh_putP]
                    print fetchD, expD_2, cur_strike
                if cur_strike - 1000000> getATMstrike: 
                    putP_atm = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = getATMstrike, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
                    sell_put = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = cur_strike, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
                    buy_putP = putP_atm.iloc[0]
                    try:
                        sell_putP = sell_put.iloc[0]
                    except:
                        sell_putP = 0
                    putP_hedge = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = hedgestrike, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
                    sellh_put = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = cur_strike+ thresh, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
                    buyh_putP = putP_hedge.iloc[0]
                    try:
                        sellh_putP = sellh_put.iloc[0]
                    except:
                        sellh_putP = 0
                    cur_strike = getATMstrike
                    pnl.loc[len(pnl)] = [fetchD, thresh,thresh2, optionType, nifty.loc[i, 'Close'], 'shift down', buy_putP, sell_putP, sell_putP - buy_putP, cur_strike,  buyh_putP - sellh_putP, buyh_putP, sellh_putP]
                    print fetchD, expD_2, cur_strike
                if fetchD==prev_expD or fetchD>prev_expD:
                    try:
                        sell_put = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = cur_strike, option_type = optionType, expiry_date = prev_expD)['Close']
                        sell_putP = sell_put.iloc[0]
                    except:
                        sell_putP = 0
                    putP_atm = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = getATMstrike, option_type = optionType, expiry_date = calc_expiry(fetchD+ timedelta(days = 1)))['Close']
                    buy_putP = putP_atm.iloc[0]
                    try:
                        sellh_put = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = cur_strike+ thresh, option_type = optionType, expiry_date = prev_expD)['Close']
                        sellh_putP = sellh_put.iloc[0]
                    except:
                        sellh_putP = 0
                    putP_h = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = hedgestrike, option_type = optionType, expiry_date = calc_expiry(fetchD+ timedelta(days = 1)))['Close']
                    buyh_putP = putP_h.iloc[0]
                    cur_strike = getATMstrike
                    pnl.loc[len(pnl)] = [fetchD, thresh, thresh2, optionType, nifty.loc[i, 'Close'],'expiry', buy_putP, sell_putP, sell_putP - buy_putP, cur_strike,  buyh_putP - sellh_putP, buyh_putP, sellh_putP]
                    prev_expD = expD
                    prev_expD_2 = expD_2
                    print fetchD, expD_2, cur_strike
                    
pnl.to_csv('hedge_strat.csv')
#    for i in map(lambda z:z/100.0, range(2, 10)):
#        thresh = i
#        if len(pnl[(pnl['thresh'] == thresh) & (pnl['Date'] == expD)]) ==0:
#            pass
#        else:
#            continue
#        try:
#            put_hedge = a(nifty.loc[prev_expD, 'Close']*(1-thresh))
#        except:
#            put_hedge = a(nifty.loc[prev_expD+timedelta(1), 'Close']*(1-thresh))            
#        putP_far = nsepy.get_history("NIFTY", prev_expD_2, endD, index= True, strike_price = put_hedge, option_type = "PE", expiry_date = calc_expiry(fetchD+timedelta(2)).date())['Close']    
#        buyH_putP = putP_far.iloc[-1]
#        sellH_putP = putP_far.iloc[0]
#        print thresh, prev_expD_2
        
#pnl['thresh'].unique
#%matplotlib qt
pnl1 = pnl_pe[pnl_pe['optionType']=='PE']
c_pnl = pnl.groupby(['thresh','thresh2'], as_index=False,sort=False).sum()
pnl1['a'] = pnl1['pnl'] + pnl1['hedgepnl']
j = pnl1[(pnl1['thresh2']==300)& (pnl1['thresh']==200)][['Date','a']]
j['y'] = j['a'].cumsum()

x = x.set_index('Date')
j = j.set_index('Date')

# asof
x['s'] = j['y'].asof(j.index)
a = x.merge(j, left_index=True, right_index=True, how='outer')


plt.plot(j['Date'], j['x'].fillna(0))
plt.plot(x['Date'], x['x'])
plt.plot(a['x_x'].fillna(method='ffill')+ a['x_y'].fillna(method='ffill'))
plt.plot(-(nifty['Close'] - nifty['Close'][0])/10.0)

pnl_pe = pnl.copy()
d = pnl[pnl['thresh']==0.02]
c_pnl['a'] = c_pnl['pnl'] + c_pnl['hedgepnl']
plt.plot(d['Date'], pnl_t.cumsum())
plt.plot(pnl1['pnl'].cumsum())

#sect1=df1['Banks - Private Sector']
#sect2=df1['Pharmaceuticals']
#sect3=df1['Refineries']
#
#w1=0.5
#w2=0.5
#w3=0.5
#nifty = nsepy.get_history("NIFTY", startD, endD, index= True)
#def f(w):
#    startD = datetime(2013, 1, 1)
#    endD = datetime.now()
#    pnl = pd.DataFrame(columns = ['Date', 'thresh', 'optionType', 'price', 'type', 'buyP', 'sellP', 'pnl', 'cur_strike'])
#    fetchD = startD
#    expD = startD.date()
#    expD_2 = startD
#    prev_expD= nifty.index[0]
#    thresh =  w[0]*1000
#    thresh2 = w[1]*1000
#    cur_strike = nifty.iloc[0]['Close']
#    optionType= 'PE'
#    print thresh, thresh2
#    sell_putP = 0
#    for i in nifty.index:
#        fetchD = i    
#        expD_2 =  calc_expiry(fetchD)
#        expD = calc_expiry(fetchD)
#        if expD>endD.date():
#            break
#        try: 
#            if optionType== 'PE':
#                getATMstrike = a(nifty.loc[i, 'Close'])
#            else:
#                getATMstrike = a(nifty.loc[i, 'Close']) + 100
#        except:
#            getATMstrike = a(nifty.loc[i, 'Close'])
#        if getATMstrike - thresh> cur_strike: 
#            putP_atm = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = getATMstrike, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
#            sell_put = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = cur_strike, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
#            buy_putP = putP_atm.iloc[0]
#            cur_strike = getATMstrike
#            try:
#                sell_putP = sell_put.iloc[0]
#            except:
#                sell_putP = 0
#            pnl.loc[len(pnl)] = [fetchD, thresh, optionType, nifty.loc[i, 'Close'], 'shift up', buy_putP, sell_putP, sell_putP - buy_putP, cur_strike]
##            print fetchD, expD_2, cur_strike
#        if cur_strike - thresh2> getATMstrike: 
#            putP_atm = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = getATMstrike, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
#            sell_put = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = cur_strike, option_type = optionType, expiry_date = calc_expiry(fetchD))['Close']
#            buy_putP = putP_atm.iloc[0]
#            cur_strike = getATMstrike
#            try:
#                sell_putP = sell_put.iloc[0]
#            except:
#                sell_putP = 0
#            pnl.loc[len(pnl)] = [fetchD, thresh, optionType, nifty.loc[i, 'Close'], 'shift down', buy_putP, sell_putP, sell_putP - buy_putP, cur_strike]
##            print fetchD, expD_2, cur_strike
#        if fetchD==prev_expD or fetchD>prev_expD:
#            try:
#                sell_put = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = cur_strike, option_type = optionType, expiry_date = prev_expD)['Close']
#                sell_putP = sell_put.iloc[0]
#            except:
#                sell_putP = 0
#            putP_atm = nsepy.get_history("NIFTY", fetchD, expD_2, index= True, strike_price = getATMstrike, option_type = optionType, expiry_date = calc_expiry(fetchD+ timedelta(days = 1)))['Close']
#            buy_putP = putP_atm.iloc[0]
#            cur_strike = getATMstrike
#            pnl.loc[len(pnl)] = [fetchD, thresh, optionType, nifty.loc[i, 'Close'],'expiry', buy_putP, sell_putP, sell_putP - buy_putP, cur_strike]
#            prev_expD = expD
##            print fetchD, expD_2, cur_strike
#    print pnl['pnl'].cumsum().iloc[-1]
#    return -pnl['pnl'].cumsum().iloc[-1]

#degree=2
#Cguess=[0,0]
##cons = ({'type': 'ineq', 'fun': lambda x:  x[1] - x[0]-0.01 })
#spo.minimize(f,Cguess,method='SLSQP',options={'disp':True})
