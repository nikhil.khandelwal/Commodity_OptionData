'''
Commodity

Get the data for the given commodity with a particular time interval

'''
import pandas
import datetime

from_date='2014-10-23' 
to_date='2014-10-29'
time_int=30
def data(file,from_date,to_date,time_int):    
    io = pandas.read_csv('C:/Users/DELL/.spyder-py3/files/'+file+'.csv')

    list=[]
    i=0    
    while(io['Date'][i]!=from_date):
        i+=1     
    t=io['Time'][i]
    tdate=datetime.datetime(int(to_date[0:4]),int(to_date[5:7]),int(to_date[8:10]))
    to_date=tdate.date()
    date=datetime.datetime(int(from_date[0:4]),int(from_date[5:7]),int(from_date[8:10]),int(t[0:2]),int(t[3:5]),int(t[6:8]))
    
    while(date.date()<to_date+datetime.timedelta(days=1)):
        if(str(date.time())==io['Time'][i] and str(date.date())==io['Date'][i]):
            list.append([io['Symbol'][i],io['Date'][i] ,io['Time'][i] ,io['Open'][i] ,io['High'][i] ,io['Low'][i] ,io['Close'][i] ,io['OpenInt'][i],io['Volume'][i]])
        a=time_int
        i+=1
        while(a>1 and io['Time'][i]==str((date+datetime.timedelta(minutes=1)).time())):
            date=date+datetime.timedelta(minutes=1)
            a-=1
            i+=1
        dt=io['Date'][i]
        tm=io['Time'][i]
        date=datetime.datetime(int(dt[0:4]),int(dt[5:7]),int(dt[8:10]),int(tm[0:2]),int(tm[3:5]),int(tm[6:8]))

    df=pandas.DataFrame(list,columns=['Symbol', 'Date', 'Time', 'Open', 'High', 'Low', 'Close', 'OpenInt', 'Volume'])    
    return df
     
l=data('Copper','2014-10-23','2014-10-29',30)    
    
    