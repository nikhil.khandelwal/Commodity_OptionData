'''
Take a list of Files as Input and update the historical data files of those list of commodity
'''
from kiteconnect import KiteConnect
import csv
from datetime import datetime, timedelta 
import pandas
from monthdelta import monthdelta

access_token = "u0q8ch8h2djz94zivx5xnvpca90oaruv"
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)
H=0
ntry=0
while(H==0 and ntry<5):
    ntry+=1
    try:
        H = kite.instruments(exchange='MCX')
    except Exception as e:
        print(e)
        
def AppendData(file_list):
    out=open('C:/Users/Administrator/Desktop/zerodha/dashboard/New folder/'+file_list+'.csv', 'ab')
    writer = csv.writer(out)
    io = pandas.read_csv('C:/Users/Administrator/Desktop/zerodha/dashboard/New folder/'+file_list+'.csv')

    from_date=io['Date'][len(io)-1]
    
    to_date_now = datetime.now() + timedelta(days=-1) 
    to_date_now = datetime.strftime(to_date_now, "%Y-%m-%d")
    if(from_date==to_date_now):
        print("File : " + file_list + " is already updated")
    else:
        def Calculate_InstrumentCode(inst_code,flag,H,a,file_list):
            while not inst_code :
                if(flag==1):
                    return 0
                    break
                else:    
                    for l in range(0,len(H)):
                        if(H[l]['tradingsymbol']==((file_list+str(a.year-2000)+a.strftime("%b")+'FUT').upper()) and str(datetime.now().date())<H[l]['expiry']):
                            inst_code=H[l]['instrument_token']
                            print(inst_code,H[l]['tradingsymbol'])
                            flag=1
                            return(int(inst_code)) 
                            break    
                a=a + monthdelta(1)            
        
        a=datetime.strptime(to_date_now, "%Y-%m-%d")
        inst_code=Calculate_InstrumentCode(0,0,H,a,file_list)        
        
        d1=datetime.strptime(to_date_now, "%Y-%m-%d")
        f1=datetime.strptime(from_date, "%Y-%m-%d")
        ndays = abs((f1 - d1).days)
        loopnum = int((ndays)/29)+1

        for i in range(0,loopnum):
            f_date=datetime.strftime(f1+timedelta(days=29*(i)+1),'%Y-%m-%d')
            t_date=datetime.strftime(f1+timedelta(days=29*(i+1)),'%Y-%m-%d')
            if(t_date>str(d1.date())):
                t_date=str(d1.date())
            H_vix=0
            ntry=0
            while(H_vix==0 and ntry<5):
                ntry+=1
                try:
                    H_vix = kite.historical(inst_code, f_date, t_date,"minute")
                except Exception:
                    H_vix = kite.historical(inst_code, f_date, t_date,"minute")
            
            try:
                H_vix[0]
            except IndexError:
                a=datetime.strptime(to_date_now, "%Y-%m-%d") + monthdelta(1)
                inst_code=Calculate_InstrumentCode(0,0,H,a,file_list)
                H_vix = kite.historical(inst_code, f_date, t_date,"minute")
                
            if not H_vix:
                print("Unable to append data for file:",file_list,"from date",f_date,"to",t_date)
            else:
                for i in range(0,len(H_vix)):
                    a=H_vix[i]['date']
                    a=a.split("T")
                    date=a[0]
                    a=a[1].split("+")
                    time=a[0]
                    if(time<'23:30:00'):
                        row=(str(io['Symbol'][0]), str(date), str(time), str(H_vix[i]['open']), str(H_vix[i]['high']), str(H_vix[i]['low']), str(H_vix[i]['close']), 0, str(H_vix[i]['volume'])) 
                        writer.writerow(row)
                    
    out.close()

file1=["CrudeOil","Zinc","Lead","Nickel","Gold","Silver","Copper","Alumini","NaturalGas"]

for i in range(0,len(file1)):
    AppendData(file1[i])
    