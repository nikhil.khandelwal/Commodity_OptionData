from kiteconnect import KiteConnect
import pandas as pd
from datetime import datetime, timedelta
import time
from utilities import calc_expiry,calc_vix,get_price
import nsepy
from time import mktime
import numpy as np

access_token = "cdg9oqe754d7pg1xqbiojxgjz7u3z3kl"
api_key = "c6y9qzf76nej1zbo"
kite = KiteConnect(api_key=api_key)
kite.set_access_token(access_token)
exch="NFO"
exch2 ="NSE"
from_date = datetime(2017, 12, 1 ,9, 15, 0)
to_date = datetime(2017, 12, 31, 9, 15, 0)

underlying='NIFTY'    

def get_pain_anyDate(underlying,fetch_date):
    """
    Input parameters
    :underlying: A string, stock option's name
    :fetch_date: A datetime, date who's data has to be calculated
    
    Output
    :Return A float, strike having minimum loss( i.e. maximum profit) 
            pain: A dataframe, complete data for all strikes values (i.e. 14) 
    
    """
    opt_maturity = calc_expiry(fetch_date)
    cur_month = datetime.strftime(opt_maturity, '%b').upper()
    cur_year = datetime.strftime(opt_maturity, "%y")
    if underlying not in ['NIFTY', 'BANKNIFTY']:
        isIndex = False
    else:
        isIndex = True
    maturity = str(cur_year)+cur_month
    length = 20
    columns = ['Strike', 'Price_PE', 'OI_PE', 'Price_CE', 'OI_CE'] 
    pain = pd.DataFrame(np.zeros((length, len(columns))),columns = columns)
    quote =  nsepy.get_history(symbol=underlying, start=fetch_date, end=fetch_date, futures=True, expiry_date=calc_expiry(fetch_date), index = True)
    ntry=4
    while(quote.empty and ntry>0):
        quote =  nsepy.get_history(symbol=underlying, start=fetch_date, end=fetch_date, futures=True, expiry_date=calc_expiry(fetch_date), index = True)
        ntry-=1
    price = quote["Close"].iloc[0]
    strike_dif = 50
    strike = str(int(int(price / strike_dif)*strike_dif) - (length/2)*strike_dif)
    
    for i in range(length):
        pain['Strike'][i] = float(strike)+ i*strike_dif
        for option in ['CE', 'PE']:
            ticker = underlying + maturity + str(int(pain['Strike'][i])) + option
#            try:
            if price<int(pain['Strike'][i]) and option =='PE':
                option = 'CE'
                ticker = underlying + maturity + str(int(pain['Strike'][i])) + option
                data = get_price(ticker, fetch_date, underlying,isIndex)
                pain['Price_PE'][i] = data['Close']- price + int(pain['Strike'][i])
            elif price>int(pain['Strike'][i]) and option =='CE': 
                option = 'PE'
                ticker = underlying + maturity + str(int(pain['Strike'][i])) + option
                data = get_price(ticker, fetch_date, underlying,isIndex)
                pain['Price_CE'][i] = data['Close'] + price - int(pain['Strike'][i])                    
            else:
                data = get_price(ticker, fetch_date, underlying,isIndex)
                pain['Price_'+option][i] = data['Close']
            pain['OI_'+option][i] = data['Open Interest']
#            except:
#                pain['Price_'+option][i] = 0
#                pain['OI_'+option][i] = 0
            time.sleep(1)
    
            
    if sum(pain['OI_CE']) == 0 or sum(pain['OI_PE']) == 0:
        return 0, pain
        
    pain['total_pain'] = 0
    return pain['Strike'][pain['total_pain'].idxmin()], pain


nftry=3
master_fo=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
f_date=t_date=from_date-timedelta(days=1)


while(master_fo.empty and nftry>0):
    while(t_date<to_date): 
        t_date=calc_expiry(f_date)
        if(t_date>to_date):
            t_date=to_date
        if(f_date>t_date):
            f_date=t_date
            
        if (master_fo.empty):
            ntry=4
            while(master_fo.empty and ntry>0):
                try:
                    master_fo=nsepy.get_history(symbol=underlying,start=f_date.date(),end=t_date.date(), index=True,futures=True,expiry_date=calc_expiry(f_date.date()))
                except AttributeError:
                    time.sleep(100)
                    pass
                ntry-=1
        else:
            mData=pd.DataFrame(data=None,index=None,columns=None,dtype=None)
            ntry=4
            while(mData.empty and ntry>0):
                try:
                    mData=nsepy.get_history(symbol=underlying,start=f_date.date(),end=t_date.date(), index=True,futures=True,expiry_date=calc_expiry(f_date.date()))
                    master_fo=pd.concat([master_fo,mData])
                except AttributeError as e:
                    pass
                ntry-=1
                f_date=t_date+timedelta(days=1)    
    
    nftry-=1
if (from_date.weekday()>4):
    curr_date=from_date+timedelta(days=(7-from_date.weekday()))
else:
    curr_date=from_date

OI=pd.DataFrame(data=None,index=None,columns=None,dtype=None)

while(curr_date<=to_date):
    while(master_fo[master_fo.index==curr_date.date()].empty and curr_date<=to_date):
        curr_date+=timedelta(days=1)
    if curr_date>to_date:    
       continue
   
    opt_maturity=calc_expiry(curr_date).replace(hour=15,minute=30)     
    prev_expiry=calc_expiry(curr_date,'prev').replace(hour=9,minute=15)     
    
    cur_month = datetime.strftime(opt_maturity, '%b').upper()
    cur_year = datetime.strftime(opt_maturity, "%y")
    maturity = str(cur_year)+cur_month
    prev_exp_str = prev_expiry.strftime('%Y-%m-%dT%H:%M:%S+0530')
    to_date_now = datetime.strftime(curr_date, "%Y-%m-%d")

    yesterday_date = curr_date - timedelta(days = 1)    
    while(master_fo[master_fo.index==yesterday_date.date()].empty and yesterday_date>=from_date-timedelta(days=10)):
        yesterday_date-=timedelta(days=1)

    yesterday_date = datetime.strftime(yesterday_date, "%Y-%m-%d")
        
    pain, OI_data = get_pain_anyDate(underlying, curr_date)

    price=master_fo[master_fo.index==curr_date.date()]['Close'].iloc[0]
    difference_in_seconds = abs(mktime(opt_maturity.timetuple()) - mktime(curr_date.timetuple()))
    years = difference_in_seconds/float(60*60*24*365)

    volpe=[]
    volce=[]
    for i in range(0,len(OI_data)):
        v2=calc_vix(price, 'Put',OI_data['Strike'][i], (years*365),OI_data['Price_PE'][i])
        volpe.append(v2)
    
    volpe=pd.DataFrame(data=volpe,index=None,columns=['Vol_PE-'+str(curr_date.date())],dtype=None)
    
    
    for j in range(0,len(OI_data)):
        v1=calc_vix(price, 'Call',OI_data['Strike'][j], (years*365),OI_data['Price_CE'][j])
        volce.append(v1)    
    
    volce=pd.DataFrame(data=volce,index=None,columns=['Vol_CE-'+str(curr_date.date())],dtype=None)
    
    dfs = [ OI_data[['Price_CE']], volce, OI_data[['Price_PE']],volpe]
    dfs=dfs[0].join(dfs[1:])
    
    OI=pd.concat([OI, dfs], axis=1)
    print(str(curr_date.date()))
    
    if (curr_date.weekday()>=4):
        curr_date=curr_date+timedelta(days=(7-curr_date.weekday()))
    else:
        curr_date=curr_date+timedelta(days=1)
OI.to_csv("OI_data("+from_date.strftime("%b")+str(from_date.year)+").csv",index=False)