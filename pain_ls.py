
#from utilities import calc_expiry, get_sector, get_strike_diff, get_price, option_pnl
import pandas as pd
import matplotlib.pyplot as plt
import math
from strategy_params import calc_strategy_params
import numpy as np
import scipy.optimize as spo

#from datetime import datetime, timedelta
#import nsepy 
#import time
#import math

pain_data=pd.read_csv("optionPain_Expiry_dates1.csv")
#pain_data['Date'] =pd.to_datetime(pain_data.Date)
#del pain_data['Unnamed: 0']
#pain_data=pain_data.sort_values(['Date','underlying'], ascending=[True, True])
#pain_data=pain_data.drop_duplicates()
#pain_data.to_csv("optionPain_Expiry_dates1.csv",index=False)
root = ''
def calc_change(a, b):
    try:
        r = (float(a)/b - 1)*100
    except:
        r = 0
    return r

pain_data['pain_price_diff'] = pain_data['Pain']/pain_data['Close']
pain_data['prev_close'] = 0
pain_data['prev_pain'] = 0
        
dates = pain_data['Date'].unique()

for i in range(1, len(dates)):
    for j in pain_data['underlying'].unique():
        index_prev = pain_data[(pain_data['Date']== dates[i-1]) &(pain_data['underlying']== j)].index
        index = pain_data[(pain_data['Date']== dates[i]) &(pain_data['underlying']== j)].index
        try:
            index[0]
            index_prev[0]
        except:
            continue
        pain_data.loc[index, 'prev_close'] = float(pain_data.loc[index_prev, 'Close'])
        pain_data.loc[index, 'prev_pain'] = float(pain_data.loc[index_prev, 'Pain'])
        

pain_data['prev_pain_price_diff'] = pain_data['prev_pain']/pain_data['prev_close']
pain_data['pnl'] = 0
pain_data['Prev_prevClose']=0
pain_data['Prev_prevPain']=0

for i in range(len(pain_data)):
    if calc_change(pain_data['prev_close'][i],pain_data['Close'][i])>=30:
        pain_data.loc[i, 'pnl']=0
    elif pain_data.loc[i, 'prev_pain_price_diff']>1:
        pain_data.loc[i, 'pnl'] = (-pain_data.loc[i,'Close'] + pain_data.loc[i,'prev_close'])/pain_data.loc[i, 'prev_close']
    elif pain_data.loc[i, 'prev_pain_price_diff']<1:
        pain_data.loc[i, 'pnl'] = (pain_data.loc[i,'Close'] - pain_data.loc[i,'prev_close'])/pain_data.loc[i, 'prev_close']
    try:
        pain_data.loc[i,'Prev_prevClose'] = pain_data.loc[i-47,'prev_close']
        pain_data.loc[i,'Prev_prevPain'] = pain_data.loc[i-47,'prev_pain']
    except Exception:
        pain_data.loc[i,'Prev_prevClose']=0
        pain_data.loc[i,'Prev_prevPain'] = 0

datalist=[]
thresh=0
while thresh <=0.020:
    pnl_thresh  = pain_data[(pain_data['prev_pain_price_diff']>1+thresh) |(pain_data['prev_pain_price_diff']<1-thresh)]
    bydate = (pnl_thresh.groupby(['Date'], as_index=False,sort=False)['pnl']).sum()

    a=calc_strategy_params(bydate['pnl'],time_period='daily')
    a['ThresholdValue']=thresh
    datalist.append(a)#.extend(thresh))
    thresh+=0.002
bydate = (pain_data.groupby(['Date'], as_index=False,sort=False)['pnl']).sum()
bysector = (pain_data.groupby(['Sector'], as_index=False,sort=False)['pnl']).sum()
###############################################################################
'''
Top 3 sectors:-    
Banks - Private Sector
Pharmaceuticals
Refineries
'''

sample = (pain_data.groupby(['Date','Sector'], as_index=False,sort=True)['pnl']).mean()
dates1=sample['Date'].unique()
df1=pd.DataFrame(data=None,index=dates1)
sector_list=sample['Sector'].unique()

for i in range(len(sector_list)):
    a=pd.DataFrame(data=sample['pnl'][sample['Sector']==sector_list[i]])
    a.index=sample['Date'][sample['Sector']==sector_list[i]]    
    df1[sector_list[i]]=a['pnl']

df1=df1.fillna(0)
sect1=df1['Banks - Private Sector']
sect2=df1['Pharmaceuticals']
sect3=df1['Refineries']

w1=0.5
w2=0.5
w3=0.5
def f(w):    
#    w=float(w/1000.0)
    w=0.0130843215222
    print w
    pnl_thresh  = pain_data[(pain_data['prev_pain_price_diff']>1+w) |(pain_data['prev_pain_price_diff']<1-w)]
    bydate = (pnl_thresh.groupby(['Date'], as_index=False,sort=False)['pnl']).mean()
    a= -calc_strategy_params(bydate['pnl'],time_period='daily')['Sharpe Ratio']
#    print a
    return a

f(14.0)
degree=0
Cguess=np.poly1d([0.0])
cons = ({'type': 'ineq', 'fun': lambda x: 0.02 - x[0] })
spo.minimize(f,Cguess,method='SLSQP',options={'disp':True}, constraints=cons)

spo.minimize_scalar(f, bounds=(2, 20), method='bounded')

#######################################################################################
'''
OPTIMIZATION
'''
def f1(n):
    print n
    pain_data1=pain_data.sort_values(['underlying','Date'], ascending=[True, True]).reset_index()
    del pain_data1['index']
    pain_data1['npnl']=0.0
    pain_data1['n_close']=0.0

    pain_data1['n_close'][0:len(pain_data1)-n]=pain_data1['Close'][n:len(pain_data1)]
    dt=pain_data1.sort_values(['Date'])['Date'].unique()[-n:]
    pain_data1=pain_data1.fillna(0)

    pain_data1.loc[pain_data1['prev_pain_price_diff']<1, 'npnl']=(pain_data1['n_close'] - pain_data1['prev_close'])/pain_data1['prev_close']
    pain_data1.loc[pain_data1['prev_pain_price_diff']>1, 'npnl']= (-pain_data1['n_close'] + pain_data1['prev_close'])/pain_data1['prev_close']
    
#    pain_data1.loc[(pain_data1['prev_close']/pain_data1['Close'] -1)<-0.3 ,'npnl']=0.0
#    pain_data1.loc[(pain_data1['prev_close']/pain_data1['Close'] -1)>0.3 ,'npnl']=0.0
    p=pain_data1.loc[(pain_data1['prev_close']/pain_data1['Close'] -1)<-0.3 ,'npnl'].index
    q=pain_data1.loc[(pain_data1['prev_close']/pain_data1['Close'] -1)>0.3 ,'npnl'].index
    for r in range(len(q)):
        if(q[r]-n<0):
            pain_data1[0:q[r]]['npnl']=0.0            
        else:
            pain_data1[q[r]-n:q[r]]['npnl']=0.0
    for s in range(len(p)):    
        if(p[r]-n<0):
            pain_data1[0:p[r]]['npnl']=0.0            
        else:
            pain_data1[p[s]-n:p[s]]['npnl']=0.0

    for k in range(0,n):
        pain_data1.loc[pain_data1['Date']==dt[k], 'n_close']=0.0
        pain_data1.loc[pain_data1['Date']==dt[k], 'npnl']=0.0

    a= -calc_strategy_params(pain_data1['npnl'],time_period='daily')['Sharpe Ratio']
    return a,pain_data1

a,pain_data1=f1(2)
Cguess=np.poly1d([0.01])
cons = ({'type': 'ineq', 'fun': lambda x: 0.05 - x[0] })
spo.minimize(f1,Cguess,method='SLSQP',options={'disp':True}, constraints=cons)

spo.minimize_scalar(f1, bounds=(1, 6), method='bounded')#,options={'maxiter': 293})

########################################################################################

bydate_Sector = (pain_data.groupby(['Date','Sector'], as_index=False,sort=True)['pnl']).sum()
dates1=bydate_Sector['Date'].unique()
df=pd.DataFrame(data=None,index=dates1)
sector_list=bydate_Sector['Sector'].unique()
for i in range(len(sector_list)):
    a=pd.DataFrame(data=bydate_Sector['pnl'][bydate_Sector['Sector']==sector_list[i]].cumsum())
    a.index=bydate_Sector['Date'][bydate_Sector['Sector']==sector_list[i]]    
    df[sector_list[i]]=a['pnl']
    plt.plot(df[sector_list[i]])


##################### DataFrame Stratergy params with 
column=['Sector','TotalProfits','Average Profit','Win Percentage','Loss Percentage','Max Drawdown','Max Drawdown Peak','Max Drawdown wrt to Average Profit','Average Profit only','Average loss only','Profit Factor','Standard Error','Sharpe Ratio']
a=[]
for i in range(len(sector_list)):
    datalist=bydate_Sector['pnl'][bydate_Sector['Sector']==sector_list[i]].reset_index()
    datalist=calc_strategy_params(datalist['pnl'],time_period='daily')
    datalist['Sector']=sector_list[i]
    a.append(datalist)
strat_params_df=pd.DataFrame(a,index=None,columns=column)


#####################bydate without Mining and Minerals Sector Data############ 
bydate1=pain_data[pain_data['Sector']!='Mining & Minerals']
bydate1 = (bydate1.groupby(['Date'], as_index=False,sort=False)['pnl']).sum()
bydate1['CF_pnl']=bydate1['pnl'].cumsum() 
plt.plot(bydate1['CF_pnl'])
calc_strategy_params(bydate1['pnl'],time_period='daily')

###############################################################################
pain_data['Prev_prevPainPriceDiff']=pain_data['Prev_prevPain']/pain_data['Prev_prevClose']
n=2
pain_data['n_close']=0
#pain_data1=pain_data.sort_values(['underlying','Date'], ascending=[True, True]).reset_index()
#del pain_data1['index']
for p in range(0,len(pain_data)):
    if(p%100==0):
        print p
    if(pain_data['Sector'][p]==pain_data['Sector'][p+n]):
        pain_data['n_close']=pain_data['Close'][p+2]
    
    if math.isnan(pain_data['Prev_prevPainPriceDiff'][p] or pain_data['prev_pain_price_diff'][p]):
        continue 
    elif(pain_data['Prev_prevPainPriceDiff'][p]<1 and pain_data['prev_pain_price_diff'][p]>1) or (pain_data['Prev_prevPainPriceDiff'][p]>1 and pain_data['prev_pain_price_diff'][p]<1): 
        pain_data['crossover'][p]=1
        
    else:
        pain_data['crossover'][p]=0

#################FIND CROSSOVER POINTS OF A SECTOR#############################
pain_data=pain_data.sort_values(['underlying','Date'], ascending=[True, True]).reset_index()
del pain_data['index']
pain_data['n_close'][0:len(pain_data)-n]=pain_data['Close'][n:]
pain_data=pain_data.sort_values(['Date','underlying'], ascending=[True, True]).reset_index()
del pain_data['index']



bydate['CF_pnl']=bydate['pnl'].cumsum() 
calc_strategy_params(bydate['pnl'],time_period='daily')
plt.plot(bydate['CF_pnl'])

#df=pd.DataFrame(datalist,columns=['ThresholdValue','TotalProfits','Average Profit','Win Percentage','Loss Percentage','Max Drawdown','Max Drawdown Peak','Max Drawdown wrt to Average Profit','Average Profit only','Average loss only','Profit Factor','Standard Error','Sharpe Ratio'])
    
total_pnl = sum(pain_data['pnl'])/len(bydate)
pain_data[pain_data['pnl']<-0.1]
bysector = (pain_data.groupby(['Sector'], as_index=False)['pnl']).sum()

pain_data=pain_data.fillna(0)
short_return = pain_data[pain_data['prev_pain_price_diff']>1].reset_index()
long_return = pain_data[pain_data['prev_pain_price_diff']<1].reset_index()
bydate_s = (short_return.groupby(['Date'], as_index=False,sort=False)['pnl']).sum()
bydate_s['CF_pnl']=bydate_s['pnl'].cumsum()
#plt.plot(bydate_s['CF_pnl'])
calc_strategy_params(bydate_s['pnl'],time_period='daily')
bydate_l = (long_return.groupby(['Date'], as_index=False,sort=False)['pnl']).sum()
bydate_l['CF_pnl']=bydate_l['pnl'].cumsum()
#plt.plot(bydate_l['CF_pnl'])
calc_strategy_params(bydate['pnl'],time_period='daily')

#pos=pain_data[pain_data['prev_pain_price_diff']>=1].groupby(['Date','Sector']).count()
#neg=pain_data[pain_data['prev_pain_price_diff']<1].groupby(['Date','Sector']).count()
#
#pos=pain_data[pain_data['prev_pain_price_diff']>=1].groupby(['Date']).count()
#neg=pain_data[pain_data['prev_pain_price_diff']<1].groupby(['Date']).count()
#
#dfs=pos[['underlying']].combine_first(neg[['Pain']])
#dfs=dfs.rename(index=str, columns={"underlying": "Positive_Pain_Price", "Pain": "Negative_Pain_Price"})
#dfs=dfs.fillna(0)
#dfs['dif'] = dfs['Positive_Pain_Price'] - dfs['Negative_Pain_Price']
#sum(dfs['dif'])