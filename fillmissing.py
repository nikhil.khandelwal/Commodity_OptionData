import pandas
import sqlite3


io = pandas.read_csv('ll.csv')
conn = sqlite3.connect('test1.db')
cursor = conn.execute("SELECT Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume from ALU ORDER BY Date,Time ASC")

i=0
while (i<len(io)):
    if(i%10==0):
        print(i)
    if (io['Open'][i]==0):
        a=0
        if(io['Open'][i+1]==0):
            a=i
            while(io['Open'][a]==0):
                a+=1
                
            for j in range(i,a):
                if(j==0):
                    conn.execute('''UPDATE ALU SET Open = ?, Close=?, Low=?, High=? WHERE Open = ? AND Date=? AND Time= ? ''',
                                 (0,str(io['Open'][a]),str(io['Open'][a]), str(io['Open'][a]),0,str(io['Date'][0]), str(io['Time'][0])))
                else:    
                    if(j==i):
                        conn.execute('''UPDATE ALU SET Open = ?, Close=?, Low=?, High=? WHERE Open = ? AND Date=? AND Time= ? ''',
                             (str(io['Close'][i-1]),str(io['Open'][a]),min(str(io['Close'][i-1]),str(io['Open'][a])), max(str(io['Close'][i-1]),str(io['Open'][a])),0,str(io['Date'][j]), str(io['Time'][j])))
                    else:    
                        conn.execute('''UPDATE ALU SET Open = ?, Close=?, Low=?, High=? WHERE Open = ? AND Date=? AND Time= ? ''',
                             (str(io['Open'][a]),str(io['Open'][a]), min(str(io['Open'][a]),str(io['Open'][a])),max(str(io['Open'][a]),str(io['Open'][a])), 0,str(io['Date'][j]), str(io['Time'][j])))
            
            i=a-1
            
        else:  
            if(i==0):
                conn.execute('''UPDATE ALU SET Open = ?, Close=?, Low=?, High=? WHERE Open = ? AND Date=? AND Time= ? ''',
                             (0,str(io['Open'][i+1]),str(io['Open'][i+1]), str(io['Open'][i+1]),0,str(io['Date'][0]), str(io['Time'][0])))
            else:
                conn.execute('''UPDATE ALU SET Open = ?, Close=?, Low=?, High=? WHERE Open = ? AND Date=? AND Time= ? ''',
                         (str(io['Close'][i-1]),str(io['Open'][i+1]),min(str(io['Close'][i-1]),str(io['Open'][i+1])),max(str(io['Close'][i-1]),str(io['Open'][i+1])), 0,str(io['Date'][i]), str(io['Time'][i])))
    i=i+1            
#conn.execute("UPDATE ALU SET Open = 0, Close=0 WHERE Time =100200")
conn.execute("UPDATE ALU SET Time=(substr(Time,1,2) || ':' || substr(Time,3,2) || ':' || substr(Time,5,2))")

conn.commit()       
     
print("DONE") 
conn.close()