
##CONVERT TIME FORMAT##
'''
import sqlite3

conn = sqlite3.connect('test.db')
cursor = conn.execute("SELECT Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume from CRD ORDER BY Date,Time ASC")

conn.execute("UPDATE CRD SET Time=(substr(Time,1,2) || ':' || substr(Time,3,2) || ':' || substr(Time,5,2))")

conn.commit()
conn.close()
'''
import csv
input = open('nickel.csv')
output = open('nickel_edit.csv', 'w+', newline='')
writer = csv.writer(output)
head=('Symbol', 'Date', 'Time', 'Open', 'High', 'Low', 'Close', 'OpenInt', 'Volume')
writer.writerow(head)
for row in csv.reader(input):
    if ('100000'<=row[2]<'233000'):
        writer.writerow(row)
input.close()
output.close()
print('DONE')