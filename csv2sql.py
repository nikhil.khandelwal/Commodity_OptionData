import sqlite3
import pandas
import datetime

io = pandas.read_csv('nickel_edit.csv')
conn = sqlite3.connect('test1.db')
cursor = conn.execute("SELECT Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume from ALU")

#CSV into Database (all together)    
for i in range(0,len(io)):
    d=str(io['Date'][i])
    date=datetime.datetime(2000+int(d[0:2]),int(d[2:4]),int(d[4:6]),0,0,0)
    date=date.date()
    conn.execute("INSERT INTO ALU (Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume) \
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",(io['Symbol'][i], str(date), str(io['Time'][i]), str(io['Open'][i]), str(io['High'][i]), str(io['Low'][i]), str(io['Close'][i]), str(io['OpenInt'][i]), str(io['Volume'][i]))); 


cursor = conn.execute("SELECT Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume from ALU ORDER BY Date,Time ASC")
conn.commit()
#cursor = conn.execute("SELECT Symbol, Date, Time, Open, High, Low, Close, OpenInt, Volume from ALU")
for row in cursor:
    print (row)

#print (pandas.read_sql_query("SELECT * FROM ALU ORDER BY Date ASC", conn))
conn.close()